//
//  EditDetailsCollectionViewCell.h
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 11/18/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "BaseCollectionViewCell.h"

@interface EditDetailsCollectionViewCell : BaseCollectionViewCell<IQDropDownTextFieldDelegate,IQDropDownTextFieldDataSource>
{

    IBOutlet UITextField *nameTf;
    IBOutlet IQDropDownTextField *endDateTf;
    IBOutlet IQDropDownTextField *startDateTf;
    IBOutlet UITextField *budgetTf;
    NSDictionary *dataSource;
    NSDate *startDate;
    NSDate *endDate;
    
}
@property (nonatomic,strong)void(^didUpdateDetails)(NSDictionary *);
-(void)setDatasource:(NSDictionary *)datasource;
@end

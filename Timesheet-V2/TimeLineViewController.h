//
//  TimeLineViewController.h
//  Timesheet-V2
//
//  Created by Arun Jangid on 13/10/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeLineViewController : BaseViewController<UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UICollectionView *daysCollectionView;
@property (weak, nonatomic) IBOutlet UITableView *timeLineTableView;
@property (strong, nonatomic) IBOutlet UILabel *timelabel;

@end

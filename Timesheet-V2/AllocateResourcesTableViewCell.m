//
//  AllocateResourcesTableViewCell.m
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 10/17/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "AllocateResourcesTableViewCell.h"

@implementation AllocateResourcesTableViewCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)prepareForReuse
{
    [super prepareForReuse];
    [self.resourceStatus setText:@"Not Assigned"];
    [self.resourceStatus setTextColor:[UIColor redColor]];
}
-(void)textField:(IQDropDownTextField *)textField didSelectItem:(NSString *)item
{
    if ([item isEqualToString:@"Not Assigned"]) {
        [self.resourceStatus setTextColor:[UIColor redColor]];
        [self.resourceStatus setText:item];
 
    }
    else
    {
    [self.resourceStatus setTextColor:[UIColor greenColor]];
    [self.resourceStatus setText:item];
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([self.resourceStatus.text isEqualToString:@"Not Assigned"]) {
        self.didDeselectResource();
    }
    else
    {
    self.didSelectResource(self.resourceStatus.text);
    }
}
@end

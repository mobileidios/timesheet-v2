//
//  AllocateResourcesViewController.h
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 10/14/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "BaseViewController.h"

@interface AllocateResourcesViewController : BaseViewController
@property(nonatomic,strong)NSMutableDictionary *project;
@property(nonatomic,strong)NSMutableDictionary *functDict;
@end

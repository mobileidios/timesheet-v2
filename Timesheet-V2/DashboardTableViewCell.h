//
//  DashboardTableViewCell.h
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 10/18/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface DashboardTableViewCell : BaseTableViewCell
@property (strong, nonatomic) IBOutlet UILabel *projectName;

@property (strong, nonatomic) IBOutlet UILabel *projectDate;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *consumedWidthConstraint;

@property (strong, nonatomic) IBOutlet UILabel *consumedLbl;

@property (strong, nonatomic) IBOutlet UIView *allocatedView;
@property (strong, nonatomic) IBOutlet UIView *consumedView;
@property (strong, nonatomic) IBOutlet UIView *projectStatus;


@end

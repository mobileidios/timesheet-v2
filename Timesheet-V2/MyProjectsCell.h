//
//  MyProjectsCell.h
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 10/20/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface MyProjectsCell : BaseTableViewCell
@property (strong, nonatomic) IBOutlet UILabel *projectName;
@property (strong, nonatomic) IBOutlet UIView *projectStatus;
@property (strong, nonatomic) IBOutlet UIView *allocatedView;
@property (strong, nonatomic) IBOutlet UIView *consumedView;
@property (strong, nonatomic) IBOutlet UILabel *startDate;
@property (strong, nonatomic) IBOutlet UILabel *consumedLbl;
@property (strong, nonatomic) IBOutlet UIView *setUpView;
@property (strong, nonatomic) IBOutlet UIView *projectStatusInView;
@property (strong, nonatomic) IBOutlet UILabel *projectNameIn;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *consumedWidthConstraint;

@property (strong, nonatomic) IBOutlet UIButton *allocateBtn;

@end

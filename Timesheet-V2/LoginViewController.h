//
//  LoginViewController.h
//  TimeSheet
//
//  Created by LRM Dev on 22/06/16.
//  Copyright © 2016 LeftRightMind. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UIView *loginView;

@end

//
//  AddActivityViewController.m
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 10/13/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "AddActivityViewController.h"

@interface AddActivityViewController ()<UITableViewDataSource,UITableViewDelegate>
{

    IBOutlet UITableView *activityTableView;
    
    IBOutlet UITextField *activityTextField;
}
@end

@implementation AddActivityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self fetchActivitiesFromFirebase];
    }
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)fetchActivitiesFromFirebase
{
    [FIRDatabaseReference fetchSingleObjectForFirstChild:ACTIVITY forType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (![snapshot.value isKindOfClass:[NSNull class]]) {
            self.datasource = snapshot.value;
            [activityTableView reloadData];
        }
        
    }];
}
- (IBAction)addBtnClicked:(id)sender
{
   
    [activityTextField resignFirstResponder];
    if ([activityTextField hasText]) {
        
        [FIRDatabaseReference addObjectForFirstChild:ACTIVITY forObject:activityTextField.text withCompletionHandler:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref, NSString *lastPath)
        {
            [self fetchActivitiesFromFirebase];
             [activityTextField setText:@""];
        }];
    }
    else
    {
        [[SessionManager sharedManager]showAlertWithMessage:@"Enter a activity name first" forAction:^{}];
    }

}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ((![textField.text length]) && [string isEqualToString:@" "])
    {
        return NO;
        
    }
    return YES;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.datasource allValues] count];
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Activities";
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [activityTableView dequeueReusableCellWithIdentifier:@"UITableViewCel"];
    cell.textLabel.text = [[self.datasource allValues] objectAtIndex:indexPath.row];
    return cell;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [FIRDatabaseReference removeObjectWithoutAutoIDForFirstChild:ACTIVITY_PATH([[self.datasource allKeys] objectAtIndex:indexPath.row]) withCompletionHandler:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref, NSString *lastPath) {
            [self fetchActivitiesFromFirebase];
        }];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

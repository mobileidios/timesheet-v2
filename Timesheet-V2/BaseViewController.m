//
//  BaseViewController.m
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 10/13/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "BaseViewController.h"
@interface BaseViewController ()


@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (IBAction)showMenu:(id)sender {
    [self.frostedViewController presentMenuViewController];
}


@end

//
//  AddFunctionsCollectionViewCell.m
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 11/18/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "AddFunctionsCollectionViewCell.h"

@implementation AddFunctionsCollectionViewCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[allFunctions allValues] count];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *funCell = [functionsTable dequeueReusableCellWithIdentifier:@"FunctionsCell" forIndexPath:indexPath];
    funCell.textLabel.text = [[allFunctions allValues] objectAtIndex:indexPath.row];;
    return funCell;

}
- (IBAction)addFunctionsClicked:(id)sender
{
    if (![functionsTable indexPathsForSelectedRows]) {
        [[SessionManager sharedManager]showAlertWithMessage:@"Please select atleast one function!" forAction:^{}];
    }
    else
    {
    for (NSIndexPath * index in [functionsTable indexPathsForSelectedRows])
    {
        NSMutableDictionary *projectFunctions = [[NSMutableDictionary alloc]init];
        [projectFunctions setValue:[[allFunctions allValues] objectAtIndex:index.row] forKey:@"name"];
        [projectFunctions setValue:@0 forKey:@"budget"];
        [projectFunctions setValue:@0 forKey:@"consumed"];
        [dataSource[@"functions"] setObject:projectFunctions forKey:[[allFunctions allKeys] objectAtIndex:index.row]];
        
    }
    _didUpdateFunctions(dataSource);
    }
}
-(void)setDatasource:(NSDictionary *)datasource
{
    dataSource = [[NSMutableDictionary alloc]initWithDictionary:datasource];
    [FIRDatabaseReference fetchSingleObjectForFirstChild:FUNCTION forType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (![snapshot.value isKindOfClass:[NSNull class]]) {
            allFunctions = [[NSMutableDictionary alloc]initWithDictionary:snapshot.value];
            [[dataSource[@"functions"] allKeys]enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
            {
                if ([[allFunctions allKeys] containsObject:obj]) {
                    
                    [allFunctions removeObjectForKey:obj];
                    
                }
                
                
            }];
            [functionsTable reloadData];
        
        
        }
    }];
}
@end

//
//  SessionManager.h
//  Timesheet-V2
//
//  Created by Arun Jangid on 13/10/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

@interface SessionManager : AFHTTPSessionManager
+(instancetype)sharedManager;
-(void)showHUD;
-(void)hideHUD;
-(void)showAlertWithMessage:(NSString*)message forAction:(void(^)(void))alertAction;
-(void)showAlertWithTitle:(NSString*)title andMessage:(NSString*)message withButtonTitle:(NSString*)buttonTitle forViewController:(UIViewController*)viewController shouldDissmissOnaction:(BOOL)shouldDismmiss forAction:(void(^)(void))alertAction;
-(void)showAlertWithTitle:(NSString*)title andMessage:(NSString*)message withButtonTitle:(NSString*)buttonTitle withCancelButton:(BOOL)isCancel forViewController:(UIViewController*)viewController shouldDissmissOnaction:(BOOL)shouldDismmiss forAction:(void(^)(void))alertAction;
-(NSArray*)daysThisWeek;
-(void)showNotification;
@end

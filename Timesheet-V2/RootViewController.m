//
//  RootViewController.m
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 10/13/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "RootViewController.h"

@interface RootViewController ()

@end

@implementation RootViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
    if([[Data sharedInstance].currentUser.userType isEqualToString:@"Admin"]){
        self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NavControllerScene"];
        self.menuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"menuController"];
    }else{
        self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TimeLineNavigation"];
        self.menuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"EmployeeMenuTableViewController"];
    }
    [self setLimitMenuViewSize:YES];
    
    self.menuViewSize = CGSizeMake([UIScreen mainScreen].bounds.size.width * 0.8, CGRectGetHeight([UIScreen mainScreen].bounds));
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

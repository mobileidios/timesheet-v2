//
//  HistoryViewController.m
//  Timesheet-V2
//
//  Created by Sonali on 10/20/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "HistoryViewController.h"
#import "DaysCollectionViewCell.h"
#import "HistoryTableViewCell.h"

@interface HistoryViewController ()
{
    NSMutableArray *timelineDataValue;
    NSMutableArray *timelineDataKey;
    __block int dayTimeTotal;
}

@end

@implementation HistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}


-(void)clearProgressaBar{
    for (UIView *view in [self.progressView subviews]){
        [view removeFromSuperview];
    }
    timelineDataValue = [[NSMutableArray alloc]init];
    timelineDataKey = [[NSMutableArray alloc]init];
    _viewArray = [[NSMutableArray alloc]init];
    _timeArray = [[NSMutableArray alloc]init];
    _progressBarWidthArray = [[NSMutableArray alloc]init];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    _timeLabel.text = @"0:00";
    _ref = [[FIRDatabase database] reference];
    [_ref keepSynced:YES];
    self.navigationController.navigationBar.backItem.title = @"";
    timelineDataValue = [[NSMutableArray alloc]init];
    timelineDataKey = [[NSMutableArray alloc]init];
    _dataArray = [[NSMutableArray alloc]init];
    _viewArray = [[NSMutableArray alloc]init];
    _timeArray = [[NSMutableArray alloc]init];
    _progressBarWidthArray = [[NSMutableArray alloc]init];
    _timeLabel.text = @"0.00";
    _dayNames = [[NSArray alloc]initWithObjects:@"Su",@"Mo",@"Tu",@"We",@"Th",@"Fr",@"Sa",nil];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    [_progressView setFrame:CGRectMake(_progressView.frame.origin.x, _progressView.frame.origin.y,0, _progressView.frame.size.height)];
    
    [_allDateOfThisWeek enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"d MMMM YYYY, EEEE"];
        if(idx == 0){
            _dateLabel.text = obj;
            _selectedDate = obj;
            NSString *date = [_allDateOfThisWeek objectAtIndex:0];
            _selectedDate = date;
            [self fetchDataFromFireBaseForDate:_selectedDate];
            NSIndexPath *path = [NSIndexPath indexPathForItem:idx inSection:0];
            DaysCollectionViewCell *cell = (DaysCollectionViewCell *)[_daysCollectionView cellForItemAtIndexPath:path];
            [cell setBackgroundColor:[UIColor whiteColor]];
            [cell.dayLabel setTextColor:[HXColor hx_colorWithHexRGBAString:@"3f4055"]];
            [_daysCollectionView selectItemAtIndexPath:path animated:YES scrollPosition:UICollectionViewScrollPositionNone];
        }
    }];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:NO];
    [_ref removeObserverWithHandle:_refHandle];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dayNames.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    DaysCollectionViewCell *cell = [_daysCollectionView dequeueReusableCellWithReuseIdentifier:@"DaysCollectionViewCellHistory" forIndexPath:indexPath];
    cell.dayLabel.text = [_dayNames objectAtIndex:indexPath.row];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((CGRectGetWidth(self.view.frame)/7),_daysCollectionView.frame.size.height);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    DaysCollectionViewCell *cell = (DaysCollectionViewCell *)[_daysCollectionView cellForItemAtIndexPath:indexPath];
    [cell setBackgroundColor:[UIColor whiteColor]];
    [cell.dayLabel setTextColor:[HXColor hx_colorWithHexRGBAString:@"3f4055"]];
    _dateLabel.text = [_allDateOfThisWeek objectAtIndex:indexPath.row];
    _selectedDate = [_allDateOfThisWeek objectAtIndex:indexPath.row];
    timelineDataValue = [[NSMutableArray alloc]init];
    timelineDataKey = [[NSMutableArray alloc]init];
    _timeLabel.text = @"00:00";
    [_progressView setFrame:CGRectMake(_progressView.frame.origin.x, _progressView.frame.origin.y,0, _progressView.frame.size.height)];
    [_historyTableView reloadData];
    [self fetchDataFromFireBaseForDate:_selectedDate];
}

-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    DaysCollectionViewCell *cell = (DaysCollectionViewCell *)[_daysCollectionView cellForItemAtIndexPath:indexPath];
    [cell setBackgroundColor:[HXColor hx_colorWithHexRGBAString:@"3f4055"]];
    [cell.dayLabel setTextColor:[UIColor whiteColor]];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArray.count>0?_dataArray.count:0;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HistoryTableViewCell"];
    if([_dataArray count] > 0)
    {
    cell.historyFinalProjectLabel.text = [[_dataArray objectAtIndex:indexPath.row] valueForKey:@"projectName"];
    cell.historyFinalRoleLabel.text = [[_dataArray objectAtIndex:indexPath.row] valueForKey:@"projectRole"];
    cell.historyFinalActivityLabel.text = [[_dataArray objectAtIndex:indexPath.row] valueForKey:@"activityName"];
        
    int hour = [[[_dataArray objectAtIndex:indexPath.row] valueForKey:@"duration"] intValue]/60;
    int min  = [[[_dataArray objectAtIndex:indexPath.row] valueForKey:@"duration"] intValue]%60;
        if(min > 9){
            cell.historyFinalTimeLabel.text = [NSString stringWithFormat:@"%d:%d",hour,min];
        }else{
            cell.historyFinalTimeLabel.text = [NSString stringWithFormat:@"%d:0%d",hour,min];
        }    
    }
    [cell layoutIfNeeded];

    return cell;    
}

-(void)fetchDataFromFireBaseForDate:(NSString *)date{

    [timelineDataValue removeAllObjects];
    [timelineDataKey removeAllObjects];
    [_dataArray removeAllObjects];
    [FIRDatabaseReference fetchSingleObjectForFirstChild:[USERS_TIMELINE([Data sharedInstance].currentUser.userUid) stringByAppendingPathComponent:_selectedDate] forType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if([snapshot exists]){
            [timelineDataValue addObjectsFromArray:[snapshot.value allValues]];
            [timelineDataKey addObjectsFromArray:[snapshot.value allKeys]];
            dayTimeTotal = 0;
            
            [timelineDataValue enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSLog(@"obj %@",obj);
                [_dataArray addObject:obj];
                dayTimeTotal = dayTimeTotal + [obj[@"duration"] intValue];
            }];
           
           int hour = dayTimeTotal/60;
           int min  = dayTimeTotal%60;
            if(min > 9){
                _timeLabel.text = [NSString stringWithFormat:@"%d:%d",hour,min];
                _timeLabel.text = [NSString stringWithFormat:@"%d:%d",hour,min];
            }else{
                _timeLabel.text = [NSString stringWithFormat:@"%d:0%d",hour,min];
                _timeLabel.text = [NSString stringWithFormat:@"%d:0%d",hour,min];
            }
             [self setProgressBarWith:hour And:min];
            [_historyTableView reloadData];
        }
    }];
     [_historyTableView reloadData];
}
-(void)setProgressBarWith:(int)hour And:(int)min{
    int timeInMinutes = (hour * 60) + min;
    int barWidth = (int)_progressViewContainer.frame.size.width;
    float percent = (timeInMinutes / 1440.0)*100.0;
    float setWidth = (percent/100)*barWidth;
    [_progressView setFrame:CGRectMake(_progressView.frame.origin.x, _progressView.frame.origin.y,setWidth, _progressView.frame.size.height)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

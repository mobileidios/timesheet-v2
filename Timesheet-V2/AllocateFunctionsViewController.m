//
//  AllocateFunctionsViewController.m
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 10/19/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "AllocateFunctionsViewController.h"
#import "AllocateFunctionsCell.h"
#import "ProjectBudgetCell.h"

@interface AllocateFunctionsViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView *allocationTable;
    NSMutableArray *functionsDict;
    NSMutableArray *functionsKey;
    UIBarButtonItem * doneBtn;
}
@end

@implementation AllocateFunctionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)doneClicked
{
    [FIRDatabaseReference addObjectWithoutAutoIDForFirstChild:ALLOCATIONDONE_PATH(self.projKey) forObject:@"YES"];
//    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    doneBtn = [[UIBarButtonItem alloc]initWithTitle:@"DONE" style:UIBarButtonItemStylePlain target:self action:@selector(doneClicked)];
    self.navigationItem.rightBarButtonItem = doneBtn;
    functionsDict = [[NSMutableArray alloc]initWithArray:[self.projDict[@"functions"]allValues]];
    functionsKey = [[NSMutableArray alloc]initWithArray:[self.projDict[@"functions"]allKeys]];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [functionsDict count] + 2;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < [functionsDict count]) {
    AllocateFunctionsCell *allocCell = [allocationTable dequeueReusableCellWithIdentifier:@"AllocateFunctionsCell" forIndexPath:indexPath];
    allocCell.funcName.text = [[functionsDict objectAtIndex:indexPath.row]valueForKey:@"name"];
        int functBudget = [[[functionsDict objectAtIndex:indexPath.row]valueForKey:@"budget"]intValue]/60;
    allocCell.funcAllocation.text = [NSString stringWithFormat:@"%d",functBudget];
    allocCell.didUpdateBudget = ^(int budget){
        if (!(budget  == [[[functionsDict objectAtIndex:indexPath.row]valueForKey:@"budget"] intValue])) {
           
            [FIRDatabaseReference addObjectWithoutAutoIDForFirstChild:FUNCTIONBUDGET_PATH(self.projKey, [functionsKey objectAtIndex:indexPath.row]) forObject:@(budget * 60)];
            ProjectBudgetCell *projCell = [allocationTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[functionsDict count] inSection:0]];
            int totalBudget = [projCell.allocatedBudgetLbl.text intValue] + budget ;
            projCell.allocatedBudgetLbl.text = [NSString stringWithFormat:@"%d",totalBudget];
        }
        self.navigationItem.rightBarButtonItem = doneBtn;

        
    };
    allocCell.hideDoneBtn = ^(){
    self.navigationItem.rightBarButtonItem = nil;
    };
    return allocCell;
    }
    else if (indexPath.row == [functionsDict count])
    {
        ProjectBudgetCell *projCell = [allocationTable dequeueReusableCellWithIdentifier:@"ProjectBudgetCell" forIndexPath:indexPath];
        int totalBudget = 00;
        for (int total = 0; total < [[functionsDict valueForKey:@"budget"]count]; total++) {
            totalBudget = totalBudget + ([[[functionsDict valueForKey:@"budget"]objectAtIndex:total]intValue]/60);
        }
        projCell.allocatedBudgetLbl.text = [NSString stringWithFormat:@"%d",totalBudget];
        projCell.totalLbl.text = @"Total Allocated(Manager)";
        return projCell;
        
    }
    else
    {
        ProjectBudgetCell *projCell = [allocationTable dequeueReusableCellWithIdentifier:@"ProjectBudgetCell" forIndexPath:indexPath];
        int totalBudget = ([[self.projDict valueForKey:@"budget"]intValue]/60);
        [projCell.allocatedBudgetLbl setTextColor:[UIColor redColor]];
        projCell.allocatedBudgetLbl.text = [NSString stringWithFormat:@"%d",totalBudget];
        projCell.totalLbl.text = @"Total Allocated(ADMIN)";
        return projCell;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  EmployeeMenuTableViewController.m
//  Timesheet-V2
//
//  Created by Arun Jangid on 14/10/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "EmployeeMenuTableViewController.h"

@interface EmployeeMenuTableViewController ()
{

    IBOutlet UILabel *employeeEmail;
    IBOutlet UILabel *employeeName;
    IBOutlet UIImageView *employeeImage;
    
    IBOutlet UIView *totalWeekView;
    __block float dayTimeTotal ;
    IBOutlet NSLayoutConstraint *consumedWidthConstraint;
    
    IBOutlet UILabel *weekHours;
}
@end

@implementation EmployeeMenuTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
-(void)viewWillAppear:(BOOL)animated
{
    [self setCurrentWeekStatusWithCompletionHandler:^{
        //NSLog(@"%d",dayTimeTotal);
    }];
    employeeName.text = [Data sharedInstance].currentUser.userName;
    employeeEmail.text = [Data sharedInstance].currentUser.userEmail;
}
-(void)setCurrentWeekStatusWithCompletionHandler:(void (^)(void))completionHandler
{
    dayTimeTotal = 0;
    NSMutableArray *dysInWeek = [[NSMutableArray alloc]initWithArray:[[SessionManager sharedManager]daysThisWeek]];
   
    for (NSString *day in dysInWeek)
    {
        [FIRDatabaseReference fetchSingleObjectForFirstChild:CURRENTWEEKSTATUS_PATH([Data sharedInstance].currentUser.userUid,day) forType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            if ([snapshot exists]) {
            [[snapshot.value allValues]enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                dayTimeTotal = dayTimeTotal + [obj[@"duration"] intValue];
                
            }];
                
        
                consumedWidthConstraint.constant = (dayTimeTotal * totalWeekView.frame.size.width)/10080 ;
                weekHours.text =[NSString stringWithFormat:@"%d : %d",(int)dayTimeTotal/60,(int)dayTimeTotal%60];
            }
        }];
        
    }
    completionHandler();
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (indexPath.row) {
        case 0:
            [self setHomeViewControllerWithStoryboardID:@"TimeLineNavigation"];
            break;
        case 1:
            [self setHomeViewControllerWithStoryboardID:@"NavMyProjectsViewController"];
            break;
        case 2:
            [self setHomeViewControllerWithStoryboardID:@"NavHistoryViewController"];
            break;
        case 3:
            [User clearUser];
            [UIApplication sharedApplication].keyWindow.rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
            break;
        default:
            break;
    }
    
}
-(void)setHomeViewControllerWithStoryboardID:(NSString*)storyboardID
{
    
    self.frostedViewController.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:storyboardID];
    [self.frostedViewController hideMenuViewController];
}
/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

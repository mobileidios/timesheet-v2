//
//  MyProjectsViewController.m
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 10/20/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "MyProjectsViewController.h"
#import "MyProjectsCell.h"
#import "DashboardProjectViewController.h"
#import "AllocateFunctionsViewController.h"
@interface MyProjectsViewController ()<UITableViewDataSource,UITableViewDelegate>
{

    IBOutlet UITableView *myProjectsTable;
    NSMutableArray *myProjects;
    NSMutableArray *projectKeys;
    
}
@end

@implementation MyProjectsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)allocateClicked:(UIButton *)sender {
    AllocateFunctionsViewController *projCon = [self.storyboard instantiateViewControllerWithIdentifier:@"AllocateFunctionsViewController"];
    projCon.projDict = [[NSMutableDictionary alloc]initWithDictionary:[myProjects objectAtIndex:sender.tag]];
    projCon.projKey = [[NSString alloc]initWithString:[projectKeys objectAtIndex:sender.tag]];
    //[self.navigationController pushViewController:projCon animated:YES];
    UINavigationController *navigationController =
    [[UINavigationController alloc] initWithRootViewController:projCon];
     navigationController.navigationBar.barTintColor = [UIColor hx_colorWithHexRGBAString:@"E87E04"];
    [self.navigationController presentViewController:navigationController animated:YES completion:nil];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self fetchProjectsForManager];
}
-(void)fetchProjectsForManager
{
    projectKeys = [[NSMutableArray alloc]init];
     myProjects = [[NSMutableArray alloc]init];
    [FIRDatabaseReference fetchSingleObjectForFirstChild:PROJECTS forType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (![snapshot.value isKindOfClass:[NSNull class]]) {
           // self.datasource = snapshot.value ;
           [[snapshot.value allValues]enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
               if((obj[@"Resources"][[Data sharedInstance].currentUser.userUid]) && ([obj[@"Resources"][[Data sharedInstance].currentUser.userUid][@"role"] isEqualToString:@"Manager"]))
               {
                   [myProjects addObject:obj];
                   [projectKeys addObject:[[snapshot.value allKeys]objectAtIndex:idx]];
               }

           }];
            [myProjectsTable reloadData];
        }
        
        
    }];

}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [myProjects count];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    NSMutableDictionary * projDict = [[NSMutableDictionary alloc]init];
    //    [projDict setObject:[[self.datasource allValues]objectAtIndex:indexPath.row] forKey:[[self.datasource allKeys]objectAtIndex:indexPath.row]];
    if (![[[myProjects objectAtIndex:indexPath.row]valueForKey:@"allocationDone"] isEqualToString:@"NO"])
    {
    DashboardProjectViewController *projCon = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardProjectViewController"];
    projCon.projDict = [[NSMutableDictionary alloc]initWithDictionary:[myProjects objectAtIndex:indexPath.row]];
        projCon.projKey = [[NSString alloc]initWithString:[projectKeys objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:projCon animated:YES];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyProjectsCell * projCell = [myProjectsTable dequeueReusableCellWithIdentifier:@"MyProjectsCell" forIndexPath:indexPath];
    if ([[[myProjects objectAtIndex:indexPath.row]valueForKey:@"allocationDone"] isEqualToString:@"NO"]) {
       projCell.projectNameIn.text = [[myProjects objectAtIndex:indexPath.row]valueForKey:@"name"];
        projCell.allocateBtn.tag = indexPath.row;
        if ([[[myProjects objectAtIndex:indexPath.row]valueForKey:@"status"] isEqualToString:@"Live"]) {
            projCell.projectStatusInView.backgroundColor = [UIColor greenColor];
        }
        else
        {
             projCell.projectStatusInView.backgroundColor = [UIColor redColor];
        }
       
    }
    else
    {
        projCell.setUpView.hidden = YES;
    projCell.projectName.text = [[myProjects objectAtIndex:indexPath.row]valueForKey:@"name"];
    projCell.startDate.text = [[myProjects objectAtIndex:indexPath.row]valueForKey:@"startDate"];
        __block float consumed = 0;
        __block float alloted = 0;
        [[[[myProjects objectAtIndex:indexPath.row]valueForKey:@"Resources"]allValues]enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSLog(@"");
            consumed = consumed + [obj[@"consumed"] floatValue];
            // = [obj[@"consumedBudget"] floatValue];
            alloted = [[[myProjects objectAtIndex:indexPath.row]valueForKey:@"budget"]floatValue] ;
            float consumedPer = (consumed/alloted) * 100 ;
            NSLog(@"%.2f",consumedPer);
            
            projCell.consumedWidthConstraint.constant = (consumedPer / 100) * projCell.allocatedView.frame.size.width ;
        }];

//    float consumed =([[[myProjects objectAtIndex:indexPath.row]valueForKey:@"consumedBudget"]floatValue]/60);
//    float alloted = ([[[myProjects objectAtIndex:indexPath.row]valueForKey:@"budget"]floatValue]/60) ;
//    float consumedPer = (consumed/alloted) * 100 ;
//    [projCell layoutIfNeeded];
//    projCell.consumedWidthConstraint.constant = (consumedPer / 100) * projCell.allocatedView.frame.size.width ;
    if ([[[myProjects objectAtIndex:indexPath.row]valueForKey:@"status"] isEqualToString:@"Live"]) {
        projCell.projectStatus.backgroundColor = [UIColor greenColor];
    }
    else
    {
        projCell.projectStatus.backgroundColor = [UIColor redColor];
    }
    
    projCell.consumedLbl.text = [NSString stringWithFormat:@"%d/%d",(int)consumed/60,(int)alloted/60];
    }
    return projCell;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}
-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
  return @"Allocate";
}
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if ([[[myProjects objectAtIndex:indexPath.row]valueForKey:@"allocationDone"] isEqualToString:@"YES"]) {
        AllocateFunctionsViewController *projCon = [self.storyboard instantiateViewControllerWithIdentifier:@"AllocateFunctionsViewController"];
        projCon.projDict = [[NSMutableDictionary alloc]initWithDictionary:[myProjects objectAtIndex:indexPath.row]];
        projCon.projKey = [[NSString alloc]initWithString:[projectKeys objectAtIndex:indexPath.row]];
        //[self.navigationController pushViewController:projCon animated:YES];
        UINavigationController *navigationController =
        [[UINavigationController alloc] initWithRootViewController:projCon];
            navigationController.navigationBar.barTintColor =[UIColor hx_colorWithHexRGBAString:@"E87E04"];
        [self.navigationController presentViewController:navigationController animated:YES completion:nil];
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

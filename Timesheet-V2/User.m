//
//  User.m
//  Timesheet-V2
//
//  Created by Arun Jangid on 13/10/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "User.h"
#import <AutoCoding/AutoCoding.h>
@implementation User
-(instancetype)init{
    self = [super init];
    if (self == nil) {
        
    }
    return self;
}

+(void)loadUser{
    [Data sharedInstance].currentUser = [User objectWithContentsOfFile:[filePath stringByAppendingPathComponent:@"User"]];
}

+(void)clearUser{
    [[NSFileManager defaultManager] removeItemAtPath:[filePath stringByAppendingPathComponent:@"User"] error:nil];
    [Data sharedInstance].currentUser = nil;
}

-(void)saveUser
{
    [self writeToFile:[filePath stringByAppendingPathComponent:@"User"] atomically:YES];
}

-(void)saveUserWithData:(id)response
{
    _userUid = response[@"uid"];
    _userEmail = response[@"email"];
    _userName = response[@"name"];
    _userType = response[@"userType"];
    [self saveUser];
}


@end

//
//  HistoryListViewController.h
//  Timesheet-V2
//
//  Created by Sonali on 10/20/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "BaseViewController.h"

@interface HistoryListViewController : BaseViewController<UITableViewDelegate ,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *historyListTableView;

@end

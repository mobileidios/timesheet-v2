//
//  HistoryViewController.h
//  Timesheet-V2
//
//  Created by Sonali on 10/20/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "BaseViewController.h"

@interface HistoryViewController : BaseViewController<UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDataSource,UITableViewDelegate>

{
    FIRDatabaseHandle _refHandle;
}
@property (weak, nonatomic) IBOutlet UICollectionView *daysCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIView *progressViewContainer;
@property (weak, nonatomic) IBOutlet UIView *progressView;
@property (weak, nonatomic) IBOutlet UITableView *historyTableView;
@property NSArray * allDateOfThisWeek; //contains whole week days and dates


@property (strong, nonatomic) FIRDatabaseReference *ref;
@property (strong, nonatomic) NSMutableArray<FIRDataSnapshot *> *dataArray;
@property NSArray *dayNames;// Su Mo Tu We Th Fr Sa
@property NSString *selectedDate;// stores selected date
@property NSMutableArray *viewArray; // contains progress bar views
@property NSMutableArray *progressBarWidthArray;//store width of progress bar views
@property NSMutableArray *timeArray; // stores

@end

//
//  DashBoardViewController.m
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 10/13/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "DashBoardViewController.h"
#import "DashboardTableViewCell.h"
#import "DashboardProjectViewController.h"

@interface DashBoardViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView *dashboardTable;
}
@end

@implementation DashBoardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self fetchProjectsFromFirebase];
    
}
-(void)fetchProjectsFromFirebase
{
    [FIRDatabaseReference fetchSingleObjectForFirstChild:PROJECTS forType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (![snapshot.value isKindOfClass:[NSNull class]]) {
            self.datasource = snapshot.value ;
           
            [dashboardTable reloadData];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSMutableDictionary * projDict = [[NSMutableDictionary alloc]init];
//    [projDict setObject:[[self.datasource allValues]objectAtIndex:indexPath.row] forKey:[[self.datasource allKeys]objectAtIndex:indexPath.row]];
    DashboardProjectViewController *projCon = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardProjectViewController"];
    projCon.projDict = [[NSMutableDictionary alloc]initWithDictionary:[[self.datasource allValues]objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:projCon animated:YES];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.datasource allValues]count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DashboardTableViewCell * dashCell = [dashboardTable dequeueReusableCellWithIdentifier:@"DashboardTableViewCell" forIndexPath:indexPath];
    dashCell.projectDate.text = [[[self.datasource allValues]objectAtIndex:indexPath.row]valueForKey:@"startDate"];
    dashCell.projectName.text = [[[self.datasource allValues]objectAtIndex:indexPath.row]valueForKey:@"name"];
    [dashCell layoutIfNeeded];
    __block float consumed = 0;
    __block float alloted = 0;
    [[[[[self.datasource allValues]objectAtIndex:indexPath.row]valueForKey:@"Resources"]allValues]enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSLog(@"");
        consumed = consumed + [obj[@"consumed"] floatValue];
      // = [obj[@"consumedBudget"] floatValue];
        alloted = [[[[self.datasource allValues]objectAtIndex:indexPath.row]valueForKey:@"budget"]floatValue] ;
        float consumedPer = (consumed/alloted) * 100 ;
        NSLog(@"%.2f",consumedPer);
        
        dashCell.consumedWidthConstraint.constant = (consumedPer / 100) * dashCell.allocatedView.frame.size.width ;
    }];
   
//    float consumed =([[[[self.datasource allValues]objectAtIndex:indexPath.row]valueForKey:@"consumedBudget"]floatValue]/60);
//    
//    float alloted = ([[[[self.datasource allValues]objectAtIndex:indexPath.row]valueForKey:@"budget"]floatValue]/60) ;
//    float consumedPer = (consumed/alloted) * 100 ;
//    NSLog(@"%.2f",consumedPer);
//    [dashCell layoutIfNeeded];
//    dashCell.consumedWidthConstraint.constant = (consumedPer / 100) * dashCell.allocatedView.frame.size.width ;
    if ([[[[self.datasource allValues]objectAtIndex:indexPath.row]valueForKey:@"status"] isEqualToString:@"Live"]) {
        dashCell.projectStatus.backgroundColor = [UIColor greenColor];
    }
    else
    {
        dashCell.projectStatus.backgroundColor = [UIColor redColor];
    }
   dashCell.consumedLbl.text = [NSString stringWithFormat:@"%d/%d",(int)consumed/60,(int)alloted/60];
    return dashCell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

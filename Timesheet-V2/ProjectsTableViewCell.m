//
//  ProjectsTableViewCell.m
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 10/14/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "ProjectsTableViewCell.h"

@implementation ProjectsTableViewCell

- (IBAction)statusChanged:(UISwitch *)sender
{
    self.changeStatusHandler(sender.isOn);
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

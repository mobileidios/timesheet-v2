//
//  HistoryListViewController.m
//  Timesheet-V2
//
//  Created by Sonali on 10/20/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "HistoryListViewController.h"
#import "HistoryListTableViewCell.h"

#import "HistoryViewController.h"
@import FirebaseDatabase;
@interface HistoryListViewController ()
{
    FIRDatabaseHandle _refHandle;
    NSMutableArray *historyDataValue;
    NSMutableArray *historyDataKey;
    NSString *cellValue;
    __block int dayTimeTotal;
}
@property NSMutableArray *timeArrayTotal;
@property (strong, nonatomic) FIRDatabaseReference *ref;
@property NSMutableDictionary *historyDatesDictionary;
@property (strong, nonatomic) NSMutableArray *dataArray;

@end

@implementation HistoryListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    historyDataKey = [[NSMutableArray alloc]init];
    historyDataValue = [[NSMutableArray alloc]init];
    _timeArrayTotal = [[NSMutableArray alloc]init];
    _ref = [[FIRDatabase database] reference];
    [_ref keepSynced:YES];
    _dataArray = [[NSMutableArray alloc]init];
    [self calculateHistoryDates];
    [self fetchDataFromFirebaseWithCompletionHandler:^{
        [_historyListTableView reloadData];
    }];
    
}

-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  8;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HistoryListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HistoryListTableViewCell"];
    
    if(_historyDatesDictionary.count > 0){
        NSMutableArray *temp = [[NSMutableArray alloc]init];
        [temp setArray:[_historyDatesDictionary objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row+1]]];
        NSDateFormatter *fmt1 = [[NSDateFormatter alloc] init];
        [fmt1 setDateFormat:@"d MMMM YYYY, EEEE"];
        NSDateFormatter *fmt2 = [[NSDateFormatter alloc] init];
        [fmt2 setDateFormat:@"MMMM d"];
        NSDateFormatter *fmt3 = [[NSDateFormatter alloc] init];
        [fmt3 setDateFormat:@"dd-MM-yyyy"];
        NSString *startDate = [fmt2 stringFromDate:[temp objectAtIndex:0]];
        NSString *endDate = [fmt2 stringFromDate:[temp objectAtIndex:6]];
        cell.submitDate.text = [fmt3 stringFromDate:[temp objectAtIndex:6]];
        cell.dotImage.image = [UIImage imageNamed:@"red_dot"];
        cell.dateLabel.text = [startDate stringByAppendingString:[NSString stringWithFormat:@" - %@",endDate]];
        NSLog(@"[temp objectAtIndex:j]%@",temp);
        if(_dataArray.count > 0){
            dayTimeTotal = 0;
            for(int i=0;i<_dataArray.count;i++){
                for(int j=0;j<= 6;j++){
                    FIRDataSnapshot *Snapshot = _dataArray[i];
                    
                        NSString *key = historyDataKey[i];
                        NSLog(@"fmt1 stringFromDate:[temp objectAtIndex:j]%@",[fmt1 stringFromDate:[temp objectAtIndex:j]]);
                        NSLog(@"key %@",key);
                        if([key isEqualToString:[fmt1 stringFromDate:[temp objectAtIndex:j]]]){
                            
                            NSMutableDictionary *dic = _dataArray[i];
                            [historyDataValue removeAllObjects];
                            [historyDataValue addObject:dic];
                        [historyDataValue enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        for(NSString *dicKey in [obj allKeys])
                        {
                            NSLog(@"data %@",[obj objectForKey:dicKey]);
                            NSDictionary *dicValue  = [obj objectForKey:dicKey];
                            dayTimeTotal = dayTimeTotal + [dicValue[@"duration"] intValue];
                            NSLog(@"data %d",dayTimeTotal);
                        }
                    }];
                            int hour = dayTimeTotal/60;
                            int min  = dayTimeTotal%60;
                            if(min > 9){
                                cell.hoursLabel.text = [NSString stringWithFormat:@"%d:%d",hour,min];
                                cell.hoursLabel.text = [NSString stringWithFormat:@"%d:%d",hour,min];
                            }else{
                                cell.hoursLabel.text = [NSString stringWithFormat:@"%d:0%d",hour,min];
                                cell.hoursLabel.text = [NSString stringWithFormat:@"%d:0%d",hour,min];
                            }
                        }
                    }
            }
        }
    }
     return cell;
}

-(void)calculateHistoryDates{
    _historyDatesDictionary = [[NSMutableDictionary alloc]init];
    
    for(int i=1; i<9;i++){
        NSDate *now = [NSDate date];
        NSCalendar *cal = [NSCalendar currentCalendar];
        // Compute beginning of current week:
        NSDate *date;
        [cal rangeOfUnit:NSCalendarUnitWeekOfMonth startDate:&date interval:NULL forDate:now];
        // Go back one week to get start of previous week:
        NSDateComponents *comp1 = [[NSDateComponents alloc] init];
        [comp1 setWeekOfMonth:-i];
        NSMutableArray *dateArray = [[NSMutableArray alloc]init];
        date = [cal dateByAddingComponents:comp1 toDate:date options:0];
        // Some output format (adjust to your needs):
        NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
        [fmt setDateFormat:@"d MMMM YYYY, EEEE"];
        // Repeatedly add one day:
        NSDateComponents *comp2 = [[NSDateComponents alloc] init];
        [comp2 setDay:1];
        for (int j = 0; j <= 6; j++) {
            NSString *text = [fmt stringFromDate:date];
            [dateArray setObject:date atIndexedSubscript:j];
            date = [cal dateByAddingComponents:comp2 toDate:date options:0];
        }
        [_historyDatesDictionary setObject:dateArray forKey:[NSString stringWithFormat:@"%d",i]];
        [_historyListTableView reloadData];
    }
}

-(void)fetchDataFromFirebaseWithCompletionHandler:(void (^)(void))completionHandler{
    [_dataArray removeAllObjects];
    
    [FIRDatabaseReference fetchSingleObjectForFirstChild:USERS_TIMELINE([Data sharedInstance].currentUser.userUid) forType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if([snapshot exists]){
            
            [historyDataKey  addObjectsFromArray:[snapshot.value allKeys]];
            cellValue = 0;
            [_dataArray addObjectsFromArray:[snapshot.value allValues]];
        }
            [_historyListTableView reloadData];
    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"HistoryViewController" sender:indexPath];
}


 -(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
     if([segue.identifier isEqualToString:@"HistoryViewController"]){
         HistoryViewController *VC = [segue destinationViewController];
         NSDateFormatter *fmt1 = [[NSDateFormatter alloc] init];
         [fmt1 setDateFormat:@"d MMMM YYYY, EEEE"];
         NSMutableArray *dateToPass = [[NSMutableArray alloc]init];
         NSArray *temp = [NSArray arrayWithArray:[_historyDatesDictionary objectForKey:[NSString stringWithFormat:@"%d",(int)[sender row]+1]]];
         for(int i=0;i<temp.count;i++){
             dateToPass[i] = [fmt1 stringFromDate:[temp objectAtIndex:i]];
         }
         VC.allDateOfThisWeek = [NSMutableArray arrayWithArray:dateToPass];
     }
 }


@end

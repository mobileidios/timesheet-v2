//
//  LoginViewController.m
//  TimeSheet
//
//  Created by LRM Dev on 22/06/16.
//  Copyright © 2016 LeftRightMind. All rights reserved.
//

#import "LoginViewController.h"

@import FirebaseAuth;
@import FirebaseDatabase;
@interface LoginViewController ()<UITextFieldDelegate>
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.emailTextField.delegate = self;
    self.passwordTextField.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (IBAction)signInButtonClicked:(id)sender {
    [self Login];
}

- (IBAction)forgotPasswordButtonClicked:(id)sender {
    [self forgotPassword];
}

-(void)Login{
    NSString *email = _emailTextField.text;
    NSString *password = _passwordTextField.text;
    if(email.length == 0 || password.length == 0)
    {
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
//                                                                       message:@"Empty credentials"
//                                                                preferredStyle:UIAlertControllerStyleAlert];
//        
//        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
//        }];
//        [alert addAction:okAction];
//        [self presentViewController:alert animated:YES completion:nil];
        [[SessionManager sharedManager]showAlertWithMessage:@"Empty Credentials" forAction:^{}];
    }
    [[SessionManager sharedManager] showHUD];
    [[FIRAuth auth] signInWithEmail:email
                           password:password
                         completion:^(FIRUser * _Nullable user, NSError * _Nullable error) {
                             if (error) {
                                 
                                 UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                                                message:error.localizedDescription
                                                                                         preferredStyle:UIAlertControllerStyleAlert];
                                 
                                 UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                                 }];
                                 [alert addAction:okAction];
                                 [self presentViewController:alert animated:YES completion:nil];
                                 [[SessionManager sharedManager] hideHUD];
                                 return;
                             }
                             __block NSString *userType;
                             [FIRDatabaseReference fetchSingleObjectForFirstChild:@"Admin" forType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                                 if([snapshot.value[@"email"] isEqualToString:user.email]){
                                    userType = @"Admin";
                                 }else{
                                    userType = @"Employee";
                                 }
                                 [FIRDatabaseReference fetchSingleObjectForFirstChild:USER_INFO(user.uid) forType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                                     NSDictionary *data = @{ @"email" :user.email,
                                                             @"uid" : user.uid,
                                                             @"name" : snapshot.value[@"name"],
                                                             @"userType" : userType
                                                             };
                                     [Data sharedInstance].currentUser = [User new];
                                     [[Data sharedInstance].currentUser saveUserWithData:data];
                                     [[SessionManager sharedManager] hideHUD];
                                     [[UIApplication sharedApplication] keyWindow].rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"RootViewController"];
                                 }];

                             }];

                             
                         }];
}

-(void)forgotPassword{
    UIAlertController *prompt =
    [UIAlertController alertControllerWithTitle:nil
                                        message:@"Email:"
                                 preferredStyle:UIAlertControllerStyleAlert];
    __weak UIAlertController *weakPrompt = prompt;
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * _Nonnull action) {
                                   UIAlertController *strongPrompt = weakPrompt;
                                   NSString *userInput = strongPrompt.textFields[0].text;
                                   if (!userInput.length)
                                   {
                                       return;
                                   }
                                   [[FIRAuth auth] sendPasswordResetWithEmail:userInput
                                                                   completion:^(NSError * _Nullable error) {
                                                                       if (error) {
                                                                           NSLog(@"%@", error.localizedDescription);
                                                                           return;
                                                                       }
                                                                   }];
                               }];
    [prompt addTextFieldWithConfigurationHandler:nil];
    [prompt addAction:okAction];
    [self presentViewController:prompt animated:YES completion:nil];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}

@end

//
//  FIRDatabaseReference+LFAdditions.h
//  LFOnDemand
//
//  Created by Arun Jangid on 9/1/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import <FirebaseDatabase/FirebaseDatabase.h>

@interface FIRDatabaseReference (LFAdditions)
+(void)fetchSingleObjectForFirstChild:(NSString*)child forType:(FIRDataEventType)eventType withBlock:(void(^)(FIRDataSnapshot * _Nonnull snapshot))block;


+(void)addObjectForFirstChild:(NSString*)firstChild forObject:(id)responseObject;

+(void)addObjectForFirstChild:(NSString*)firstChild forObject:(id)responseObject withCompletionHandler:(void(^)(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref,NSString * lastPath))completionHandler;
+(void)addObjectWithoutAutoIDForFirstChild:(NSString*)firstChild forObject:(id)responseObject;

+(void)addObjectWithoutAutoIDForFirstChild:(NSString*)firstChild forObject:(id)responseObject withCompletionHandler:(void(^)(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref,NSString * lastPath))completionHandler;

+(void)fetchDataObjectForChild:(NSString*)firstChild forType:(FIRDataEventType)eventType withBlock:(void(^)(FIRDataSnapshot * _Nonnull snapshot))block;
+(void)removeObjectWithoutAutoIDForFirstChild:(NSString*)firstChild withCompletionHandler:(void(^)(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref,NSString * lastPath))completionHandler;
@end

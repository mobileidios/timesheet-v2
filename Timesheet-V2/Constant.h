//
//  Constant.h
//  LFOnDemand
//
//  Created by Arun Jangid on 8/5/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#ifndef Constant_h
#define Constant_h
#define  topView [[[UIApplication sharedApplication] windows] firstObject]
#define filePath [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES) firstObject]
#define USER_INFO(id) [NSString stringWithFormat:@"Users/%@",id]
#define FUNCTION [NSString stringWithFormat:@"Functions"]
#define ACTIVITY [NSString stringWithFormat:@"Activity"]
#define PROJECTS [NSString stringWithFormat:@"Projects"]
#define USERS [NSString stringWithFormat:@"Users"]
#define CHANGE_STATUS(id) [NSString stringWithFormat:@"Projects/%@/status",id]
#define TIMELINE [NSString stringWithFormat:@"Timeline"]
#define USERS_TIMELINE(id) [NSString stringWithFormat:@"Timeline/%@",id]
#define PROJECT_PATH(id) [NSString stringWithFormat:@"Projects/%@",id]
#define ACTIVITY_PATH(id) [NSString stringWithFormat:@"Activity/%@",id]
#define FUNCTION_PATH(id) [NSString stringWithFormat:@"Functions/%@",id]
#define FUNCTIONBUDGET_PATH(id,id1) [NSString stringWithFormat:@"Projects/%@/functions/%@/budget",id,id1]
#define ALLOCATIONDONE_PATH(id) [NSString stringWithFormat:@"Projects/%@/allocationDone",id]
#define CURRENTWEEKSTATUS_PATH(id,id1) [NSString stringWithFormat:@"Timeline/%@/%@",id,id1]

#endif 

//
//  AllocateFunctionsViewController.h
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 10/19/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "BaseViewController.h"

@interface AllocateFunctionsViewController : BaseViewController
@property(nonatomic,strong)NSMutableDictionary *projDict;
@property(nonatomic,strong)NSString *projKey;
@end

//
//  DaysCollectionViewCell.h
//  TimeSheet
//
//  Created by LRM Dev on 22/06/16.
//  Copyright © 2016 LeftRightMind. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DaysCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;

@end

//
//  FIRDatabaseReference+LFAdditions.m
//  LFOnDemand
//
//  Created by Arun Jangid on 9/1/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "FIRDatabaseReference+LFAdditions.h"
#import <AFNetworking/AFNetworkReachabilityManager.h>


@implementation FIRDatabaseReference (LFAdditions)

+(void)fetchSingleObjectForFirstChild:(NSString*)child forType:(FIRDataEventType)eventType withBlock:(void(^)(FIRDataSnapshot * _Nonnull snapshot))block{
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    
    if (!([[AFNetworkReachabilityManager sharedManager] networkReachabilityStatus] == AFNetworkReachabilityStatusNotReachable)) {
        [[SessionManager sharedManager] showHUD];
        
        [[[[FIRDatabase database] reference] child:child] observeSingleEventOfType:eventType withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            block(snapshot);
            [[SessionManager sharedManager] hideHUD];
        }];
    }else{
        
        [[SessionManager sharedManager] showAlertWithMessage:@"There is no Network Connectivity. Please check the Network Connectivity and then try again." forAction:^{
            
            
        }];
        
        
    }
    
    
}


+(void)addObjectForFirstChild:(NSString*)firstChild forObject:(id)responseObject
{
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if (!([[AFNetworkReachabilityManager sharedManager] networkReachabilityStatus] == AFNetworkReachabilityStatusNotReachable) ) {
        [[[[[FIRDatabase database] reference] child:firstChild] childByAutoId] setValue:responseObject];
        [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
        
    }else{
        [[SessionManager sharedManager] showAlertWithMessage:@"There is no Network Connectivity. Please check the Network Connectivity and then try again." forAction:^{
            
            
        }];
    }
    
}

+(void)addObjectWithoutAutoIDForFirstChild:(NSString*)firstChild forObject:(id)responseObject
{
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    if (!([[AFNetworkReachabilityManager sharedManager] networkReachabilityStatus] == AFNetworkReachabilityStatusNotReachable) ) {
        [[[[FIRDatabase database] reference] child:firstChild] setValue:responseObject];
        
        
    }else{
        [[SessionManager sharedManager] showAlertWithMessage:@"There is no Network Connectivity. Please check the Network Connectivity and then try again." forAction:^{
            [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
            
        }];
    }
    
}


+(void)addObjectWithoutAutoIDForFirstChild:(NSString*)firstChild forObject:(id)responseObject withCompletionHandler:(void(^)(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref,NSString * lastPath))completionHandler{
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    if (!([[AFNetworkReachabilityManager sharedManager] networkReachabilityStatus] == AFNetworkReachabilityStatusNotReachable) ) {
        [[[[FIRDatabase database] reference] child:firstChild] setValue:responseObject withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
            completionHandler(error,ref,[[[NSString stringWithFormat:@"%@",ref] componentsSeparatedByString:@"/"] lastObject]);
            [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
            
        }];
    }else{
        [[SessionManager sharedManager] showAlertWithMessage:@"There is no Network Connectivity. Please check the Network Connectivity and then try again." forAction:^{
            [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
            
        }];
    }
    
}


+(void)addObjectForFirstChild:(NSString*)firstChild forObject:(id)responseObject withCompletionHandler:(void(^)(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref,NSString * lastPath))completionHandler{
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    if (!([[AFNetworkReachabilityManager sharedManager] networkReachabilityStatus] == AFNetworkReachabilityStatusNotReachable) ) {
        
        [[[[[FIRDatabase database] reference] child:firstChild] childByAutoId] setValue:responseObject withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
            completionHandler(error,ref,[[[NSString stringWithFormat:@"%@",ref] componentsSeparatedByString:@"/"] lastObject]);
            [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
            
        }];
    }else{
        [[SessionManager sharedManager] showAlertWithMessage:@"There is no Network Connectivity. Please check the Network Connectivity and then try again." forAction:^{
            [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
            
        }];
    }
    
}

+(void)removeObjectWithoutAutoIDForFirstChild:(NSString*)firstChild withCompletionHandler:(void(^)(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref,NSString * lastPath))completionHandler{
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    if (!([[AFNetworkReachabilityManager sharedManager] networkReachabilityStatus] == AFNetworkReachabilityStatusNotReachable) ) {
        [[[[FIRDatabase database] reference] child:firstChild] removeValueWithCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
            completionHandler(error,ref,[[[NSString stringWithFormat:@"%@",ref] componentsSeparatedByString:@"/"] lastObject]);
            [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
            
        }];
    }else{
        [[SessionManager sharedManager] showAlertWithMessage:@"There is no Network Connectivity. Please check the Network Connectivity and then try again." forAction:^{
            [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
            
        }];
    }
    
}

+(void)fetchDataObjectForChild:(NSString*)firstChild forType:(FIRDataEventType)eventType withBlock:(void(^)(FIRDataSnapshot * _Nonnull snapshot))block{
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    if (!([[AFNetworkReachabilityManager sharedManager] networkReachabilityStatus] == AFNetworkReachabilityStatusNotReachable) ) {
        
        
        [[[[FIRDatabase database] reference] child:firstChild]observeEventType:eventType withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            block(snapshot);
            [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
            
        }];
    }else{
        [[SessionManager sharedManager] showAlertWithMessage:@"There is no Network Connectivity. Please check the Network Connectivity and then try again." forAction:^{
            [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
            
        }];
    }
    
    
}



@end


//
//  NewProjectDetailsViewController.m
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 10/14/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "NewProjectDetailsViewController.h"
#import "ChooseFunctionsViewController.h"

@interface NewProjectDetailsViewController ()<IQDropDownTextFieldDelegate,IQDropDownTextFieldDataSource>
{

    NSDate *startDate;
    NSDate *endDate;
    IBOutlet UITextField *projectBudgetField;
    IBOutlet UITextField *projectNameField;
    IBOutlet IQDropDownTextField *startDateField;
    
    IBOutlet IQDropDownTextField *endDateField;
}
@end

@implementation NewProjectDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
     [self setDates];
    startDate = [NSDate date];
    endDate = [NSDate date];
    // Do any additional setup after loading the view.
}

-(void)textField:(IQDropDownTextField *)textField didSelectDate:(NSDate *)date
{
    if (textField == startDateField) {
        startDate = date;
       [endDateField setMinimumDate:date];
    }
    else
    {
        endDate = date;
    }
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ((textField == endDateField ) && (![startDateField hasText])) {
        [[SessionManager sharedManager]showAlertWithMessage:@"Please select start date first !" forAction:^{
        
        }];
    }
}
-(void)setDates
{
    [startDateField setDropDownMode:IQDropDownModeDatePicker];
    [endDateField setDropDownMode:IQDropDownModeDatePicker];
    [startDateField setMinimumDate:[NSDate date]];
}
- (IBAction)nextBtnClicked:(id)sender {
    if ([projectNameField.text length] && [projectBudgetField.text length] && [startDateField hasText] && [endDateField hasText]) {
        ChooseFunctionsViewController *choController = [self.storyboard instantiateViewControllerWithIdentifier:@"ChooseFunctionsViewController"];
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MMM-YYYY"];
        NSDateFormatter *firebaseDateFormatter=[[NSDateFormatter alloc] init];
        [firebaseDateFormatter setDateFormat:@"d MMMM YYYY, EEEE"];
        NSString *startDateString =[firebaseDateFormatter stringFromDate:startDate];
        NSString *endDateString =[firebaseDateFormatter stringFromDate:endDate];
        NSMutableDictionary * projectParams = (NSMutableDictionary *)@{@"name":projectNameField.text,
                                         @"budget" : @([projectBudgetField.text intValue] * 60),
                                         @"startDate" :startDateString ,
                                         @"endDate" : endDateString,
                                         @"consumedBudget":@0,
                                         @"status" :@"Live"
                                         };
        choController.project =[[NSMutableDictionary alloc]initWithDictionary:projectParams];
        [self.navigationController pushViewController:choController animated:YES];
    }
    else
    {
        [[SessionManager sharedManager]showAlertWithMessage:@"Please fill all the fields properly!!" forAction:^{}];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

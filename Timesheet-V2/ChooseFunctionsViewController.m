//
//  ChooseFunctionsViewController.m
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 10/14/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "ChooseFunctionsViewController.h"
#import "AllocateResourcesViewController.h"

@interface ChooseFunctionsViewController ()<UITableViewDataSource,UITableViewDelegate>
{

    IBOutlet UITableView * functionsTable;
    NSMutableArray * selectedFunctions;
    NSMutableArray *selectedFunctKeys;
    NSMutableDictionary *functionsDict;
    NSMutableDictionary *selectedFunct;
}
@end

@implementation ChooseFunctionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    selectedFunct = [[NSMutableDictionary alloc]init];
    functionsDict = [[NSMutableDictionary alloc]init];
    [self fetchFunctionsFromFirebase];
}
-(void)fetchFunctionsFromFirebase
{
    [FIRDatabaseReference fetchSingleObjectForFirstChild:FUNCTION forType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (![snapshot.value isKindOfClass:[NSNull class]]) {
            self.datasource = snapshot.value;
            [functionsTable reloadData];
        }
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // NSLog(@"%@",[[functionSnapshot.value allKeys] objectAtIndex:indexPath.row]);
   // [selectedFunctions addObject:[self.datasource objectAtIndex:indexPath.row]];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.datasource allValues] count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [functionsTable dequeueReusableCellWithIdentifier:@"FunctionsCell" forIndexPath:indexPath];
    cell.textLabel.text = [[self.datasource allValues] objectAtIndex:indexPath.row];
    return cell;
}
- (IBAction)allocateClicked:(id)sender {
    if (![functionsTable indexPathsForSelectedRows]) {
        [[SessionManager sharedManager]showAlertWithMessage:@"Please select atleast one function!" forAction:^{}];
    }
    else
    {
       
        for (NSIndexPath * index in [functionsTable indexPathsForSelectedRows])
        {
             NSMutableDictionary *projectFunctions = [[NSMutableDictionary alloc]init];
            [projectFunctions setValue:[[self.datasource allValues] objectAtIndex:index.row] forKey:@"name"];
            [projectFunctions setValue:@0 forKey:@"budget"];
            [projectFunctions setValue:@0 forKey:@"consumed"];
            [selectedFunct setObject:[[self.datasource allValues] objectAtIndex:index.row] forKey:[[self.datasource allKeys]objectAtIndex:index.row]];
            [selectedFunctKeys addObject:[[self.datasource allKeys]objectAtIndex:index.row]];
            [selectedFunctions addObject:[[self.datasource allValues] objectAtIndex:index.row]];
            [functionsDict setObject:projectFunctions forKey:[[self.datasource allKeys] objectAtIndex:index.row]];
            
        }
        
         AllocateResourcesViewController *reController = [self.storyboard instantiateViewControllerWithIdentifier:@"AllocateResourcesViewController"];
        [self.project setObject:functionsDict forKey:@"functions"];
        reController.functDict = [[NSMutableDictionary alloc]initWithDictionary:selectedFunct];
        reController.project = [[NSMutableDictionary alloc]initWithDictionary:self.project];
        [self.navigationController pushViewController:reController animated:YES];
    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  EditProjectViewController.h
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 11/18/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "BaseViewController.h"

@interface EditProjectViewController : BaseViewController
@property(nonatomic,strong)NSString *projectKey;
@end

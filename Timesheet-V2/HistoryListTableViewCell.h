//
//  HistoryListTableViewCell.h
//  Timesheet-V2
//
//  Created by Sonali on 10/20/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryListTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *hoursLabel;
@property (strong, nonatomic) IBOutlet UIImageView *dotImage;
@property (strong, nonatomic) IBOutlet UILabel *submitDate;

@end

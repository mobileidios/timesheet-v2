//
//  BaseCollectionViewCell.m
//  Timesheet-V2
//
//  Created by Arun Jangid on 13/10/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "BaseCollectionViewCell.h"

@implementation BaseCollectionViewCell
-(void)didMoveToSuperview{
    [super didMoveToSuperview];
    
    [self setNeedsLayout];
    [self layoutIfNeeded];
}
@end

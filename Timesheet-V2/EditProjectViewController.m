//
//  EditProjectViewController.m
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 11/18/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "EditProjectViewController.h"
#import "EditDetailsCollectionViewCell.h"
#import "AddFunctionsCollectionViewCell.h"
#import "AddResourcesCollectionViewCell.h"

@interface EditProjectViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
{

    IBOutlet UICollectionView *projectCollectionView;
    
    IBOutlet UISegmentedControl *optionSegment;
}
@end

@implementation EditProjectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   //  [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self fetchProjectDataFromFirebase];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)fetchProjectDataFromFirebase
{
    [FIRDatabaseReference fetchSingleObjectForFirstChild:PROJECT_PATH(self.projectKey) forType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (![snapshot.value isKindOfClass:[NSNull class]]) {
            self.datasource = snapshot.value;
            [projectCollectionView reloadData];
        }
        
    }];
}

- (IBAction)segmentChanged:(UISegmentedControl *)sender
{
    NSIndexPath *index = [NSIndexPath indexPathForRow:sender.selectedSegmentIndex inSection:0];
    [projectCollectionView scrollToItemAtIndexPath:index atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 3;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
    EditDetailsCollectionViewCell *editDetail = [projectCollectionView dequeueReusableCellWithReuseIdentifier:@"EditDetailsCollectionViewCell" forIndexPath:indexPath];
    [editDetail setDatasource:self.datasource];
    editDetail.didUpdateDetails = ^(NSDictionary * project)
    {
    [FIRDatabaseReference addObjectWithoutAutoIDForFirstChild:PROJECT_PATH(self.projectKey) forObject:project];
        [self.navigationController popViewControllerAnimated:YES];
    };
    return editDetail;
    }
    else if (indexPath.row == 1)
    {
        AddFunctionsCollectionViewCell *funcCell = [projectCollectionView dequeueReusableCellWithReuseIdentifier:@"AddFunctionsCollectionViewCell" forIndexPath:indexPath];
        [funcCell setDatasource:self.datasource];
        funcCell.didUpdateFunctions = ^(NSDictionary * project)
        {
            [FIRDatabaseReference addObjectWithoutAutoIDForFirstChild:PROJECT_PATH(self.projectKey) forObject:project];
            [projectCollectionView reloadData];
            [projectCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
            [optionSegment setSelectedSegmentIndex:2];
        };
        return funcCell;
    }
    else
    {
        AddResourcesCollectionViewCell *resCell = [projectCollectionView dequeueReusableCellWithReuseIdentifier:@"AddResourcesCollectionViewCell" forIndexPath:indexPath];
        [resCell setDatasource:self.datasource];
        resCell.didUpdateResources = ^(NSDictionary * project)
        {
            [FIRDatabaseReference addObjectWithoutAutoIDForFirstChild:PROJECT_PATH(self.projectKey) forObject:project];
            [self.navigationController popViewControllerAnimated:YES];
        };
        return resCell;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(projectCollectionView.frame.size.width,projectCollectionView.frame.size.height);
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

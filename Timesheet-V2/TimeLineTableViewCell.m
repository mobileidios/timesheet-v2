//
//  TimeLineTableViewCell.m
//  Timesheet-V2
//
//  Created by Sonali on 10/14/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "TimeLineTableViewCell.h"
#import "ProjectNameCollectionViewCell.h"
#import "ActivityCollectionViewCell.h"

@implementation TimeLineTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _selectTimeSlider.value = 0 ;
    _selectTimeSlider.minimumValue = 0;
    _selectTimeSlider.maximumValue = 24;
    _projectListArray = [[NSMutableArray alloc] init];
    _projectListKeyArray = [[NSMutableArray alloc] init];
    _activityListArray = [[NSMutableArray alloc]init];
    _timeLine = [[NSMutableDictionary alloc]init];
    self.selectTimeView.hidden = YES;
    self.selectActivityView.hidden = YES;
    self.selectProjectView.hidden = YES;
    self.addButtonView.hidden = YES;
    self.finalView.hidden = YES;
    [_selectTimeSlider setValue:0];
    [self fetchDataFromFirebase];
}

-(void)prepareForReuse{
    _selectTimeSlider.value = 0 ;
    self.selectTimeLabel.text = @"0:00";
    self.selectTimeView.hidden = YES;
    self.selectActivityView.hidden = YES;
    self.selectProjectView.hidden = YES;
    self.addButtonView.hidden = YES;
    self.finalView.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if(collectionView == _selectProjectCollectionView){
        return [_projectListArray count];
    }else{
        return [_activityListArray count];
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(collectionView == _selectProjectCollectionView)
    {
        ProjectNameCollectionViewCell *cell = [_selectProjectCollectionView dequeueReusableCellWithReuseIdentifier:@"ProjectNameCollectionViewCell" forIndexPath:indexPath];
        cell.projectNameLabel.text = _projectListArray[indexPath.row][@"name"];
        [cell layoutIfNeeded];
        return cell;
    }
    else if(collectionView == _selectActivityCollectionView)
    {
        ActivityCollectionViewCell *cell = [_selectActivityCollectionView dequeueReusableCellWithReuseIdentifier:@"ActivityCollectionViewCell" forIndexPath:indexPath];
        cell.activityLabel.text = _activityListArray[indexPath.row];
        return cell;
    }
    return  nil;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(collectionView == _selectProjectCollectionView){
        self.selectProjectView.hidden = YES;
        self.selectActivityView.hidden = NO;
        self.addButtonView.hidden = YES;
        self.finalView.hidden = YES;
        [_timeLine setValue:_projectListArray[indexPath.row][@"name"] forKey:@"projectName"];
        [_timeLine setValue:_projectListKeyArray[indexPath.row] forKey:@"projectId"];
        _finalViewProjectLabel.text = _projectListArray[indexPath.row][@"name"];
        [_timeLine setValue:_projectListArray[indexPath.row][@"Resources"][[Data sharedInstance].currentUser.userUid][@"role"] forKey:@"projectRole"];
        [_timeLine setValue:_projectListArray[indexPath.row][@"Resources"][[Data sharedInstance].currentUser.userUid][@"roleKey"] forKey:@"projectRoleKey"];
    }
    else if(collectionView == _selectActivityCollectionView){
        self.selectActivityView.hidden = YES;
        self.selectProjectView.hidden = YES;
        self.selectTimeView.hidden = NO;
        self.addButtonView.hidden = YES;
        self.finalView.hidden = YES;
        [_timeLine setValue:[_activityListArray objectAtIndex:indexPath.row] forKey:@"activityName"];
        _finalViewActivityLabel.text = [_activityListArray objectAtIndex:indexPath.row];
    }
}

- (IBAction)selectTimeSlider:(id)sender {
    
        int value = 0;
        [_selectTimeSlider setValue:((int)((_selectTimeSlider.value) / 0.25) * 0.25) animated:NO];
        NSArray* timeArray = [[NSString stringWithFormat:@"%.2f", _selectTimeSlider.value] componentsSeparatedByString: @"."];
        NSString* hourValue = [timeArray objectAtIndex: 0];
        int min = ([[timeArray objectAtIndex:1] intValue]/100.0)*60;
        NSString *minuteValue =  [NSString stringWithFormat:@"%d", min];
        int hour = [[timeArray objectAtIndex:0] intValue] * 60;
        int minutes = ([[timeArray objectAtIndex:1] intValue] * 60)/100;
    
        int previousTime =  self.addtionOfPreviousTime();          // Addition of previous time if new project is added
        if(previousTime != 0)
        {
             value = (self.addtionOfPreviousTime() + hour + minutes);
        }
        else
        {
          value = (self.check24HourHandler() + hour + minutes);
        }
        if(value  < 1440){
        if(min>9){
            self.updateTimeLabel([NSString stringWithFormat:@"%@:%@", hourValue,minuteValue]);
        }else{
            self.updateTimeLabel([NSString stringWithFormat:@"%@:0%@", hourValue,minuteValue]);
        }
        [self.selectTimeSlider addTarget:self action:@selector(dragEndedForSlider:)forControlEvents:UIControlEventTouchUpInside];
    }else {}
}

- (IBAction)addButton:(id)sender {
    self.addButtonView.hidden = YES;
    self.selectTimeView.hidden = YES;
    self.selectActivityView.hidden = YES;
    self.selectProjectView.hidden = NO;
}

-(void)dragEndedForSlider:(id)sender
{
    int hour = 0;
    int min = 0;
    hour = [[[self.selectTimeLabel.text componentsSeparatedByString:@":"] firstObject] intValue];
    min = [[[self.selectTimeLabel.text componentsSeparatedByString:@":"] lastObject] intValue];
    int time = min + (hour*60);
    [_timeLine setValue:@(time) forKey:@"duration"];
    self.finalViewTimeLabel.text = self.selectTimeLabel.text;
    self.selectActivityView.hidden = YES;
    self.selectProjectView.hidden = YES;
    self.selectTimeView.hidden = YES;
    self.addButtonView.hidden = YES;
    self.finalView.hidden = NO;
    if([_timeLine count] > 1){
        self.addTimeLine(_timeLine);
         _timeLine = [[NSMutableDictionary alloc]init];
    }else{
        self.updateTimeLineDuration(time);
        self.timeEqual24Hour();
    }
}

-(void)fetchDataFromFirebase{
    [_projectListArray removeAllObjects];
    [_activityListArray removeAllObjects];
    [FIRDatabaseReference fetchSingleObjectForFirstChild:PROJECTS forType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if(![_projectListArray containsObject:snapshot]){
            for (FIRDataSnapshot *snap in snapshot.children.allObjects) {
                    if(!(snap.value[@"Resources"][[Data sharedInstance].currentUser.userUid] == nil)){
                        [_projectListArray addObject:snap.value];
                        [_projectListKeyArray addObject:snap.key];
                        [_selectProjectCollectionView reloadData];
                    }
            }
        }
    }];
    [FIRDatabaseReference fetchSingleObjectForFirstChild:ACTIVITY forType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if(![_activityListArray containsObject:snapshot]){
            _activityListArray = [snapshot.value allValues];
            [_selectActivityCollectionView reloadData];
        }
    }];
}
@end

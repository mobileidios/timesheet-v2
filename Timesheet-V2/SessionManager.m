//
//  SessionManager.m
//  Timesheet-V2
//
//  Created by Arun Jangid on 13/10/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "SessionManager.h"

static SessionManager * manager = nil;
@implementation SessionManager

+(instancetype)sharedManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        if (!manager) {
            manager = [[SessionManager alloc]init];
            
            //Request
            [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
            [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            
            //Response
            [manager setResponseSerializer:[AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments]];
            
            [[AFNetworkActivityLogger sharedLogger] startLogging];
            [[AFNetworkActivityLogger sharedLogger] setLevel:AFLoggerLevelDebug];
            
        }        
    });
    return manager;
}

-(void)showHUD
{
    if (![MBProgressHUD HUDForView:topView]) {
        [MBProgressHUD showHUDAddedTo:topView animated:YES];
    }
}

-(void)hideHUD
{
    
    if (![MBProgressHUD HUDForView:topView ]) {
        
    }else{
        [MBProgressHUD hideHUDForView:topView animated:YES];
    }
}


-(void)showAlertWithTitle:(NSString*)title andMessage:(NSString*)message withButtonTitle:(NSString*)buttonTitle forViewController:(UIViewController*)viewController shouldDissmissOnaction:(BOOL)shouldDismmiss forAction:(void(^)(void))alertAction
{
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:buttonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (shouldDismmiss){
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }
        alertAction();
    }]];
    
    [viewController presentViewController:alertController animated:YES completion:nil];
}

-(void)showAlertWithTitle:(NSString*)title andMessage:(NSString*)message withButtonTitle:(NSString*)buttonTitle withCancelButton:(BOOL)isCancel forViewController:(UIViewController*)viewController shouldDissmissOnaction:(BOOL)shouldDismmiss forAction:(void(^)(void))alertAction{
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:buttonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (shouldDismmiss){
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }
        alertAction();
    }]];
    
    if (isCancel) {
        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }]];
    }
    [viewController presentViewController:alertController animated:YES completion:nil];
    
}

-(void)showAlertWithMessage:(NSString*)message forAction:(void(^)(void))alertAction
{
    
    [self showAlertWithTitle:@"" andMessage:message withButtonTitle:@"Ok" forViewController:[topView rootViewController]  shouldDissmissOnaction:YES forAction:^{
        alertAction();
    }];
}

-(NSArray*)daysThisWeek
{
    return  [self daysInWeek:0 fromDate:[NSDate date]];
}

-(NSArray*)daysInWeek:(int)weekOffset fromDate:(NSDate*)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    //ask for current week
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    comps=[calendar components:NSWeekCalendarUnit|NSCalendarUnitYear fromDate:date];
    // comps=[calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear fromDate:date];
    //create date on week start
    NSDate* weekstart=[calendar dateFromComponents:comps];
    NSDateComponents* moveWeeks=[[NSDateComponents alloc] init];
    moveWeeks.weekOfYear=weekOffset;
    weekstart=[calendar dateByAddingComponents:moveWeeks toDate:weekstart options:0];
    //add 7 days
    NSMutableArray* week=[NSMutableArray arrayWithCapacity:7];
    for (int i=0; i<7; i++) {
        NSDateComponents *compsToAdd = [[NSDateComponents alloc] init];
        compsToAdd.day=i;
        NSDate *nextDate = [calendar dateByAddingComponents:compsToAdd toDate:weekstart options:0];
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"d MMMM YYYY, EEEE"];
        [week addObject:[formatter stringFromDate:nextDate]];
    }
    return [NSArray arrayWithArray:week];
}
-(void)showNotification
{
    
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSArray *currentTime = [[dateFormatter stringFromDate:today] componentsSeparatedByString:@" "];
    NSArray *currentHourMinute = [[currentTime objectAtIndex:0] componentsSeparatedByString:@":"];
    float hour = [[currentHourMinute objectAtIndex:0] floatValue]*60;
    float minute = [[currentHourMinute objectAtIndex:1] floatValue];
    if((1080 - (hour+minute))>0){        // 17 * 60 = 1080 i.e 5 pm
        int time = (1080 - (hour+minute))*60;
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:time];
        localNotification.alertBody= @"Please Fill TimeSheet";
        localNotification.repeatInterval = 0;
        localNotification.soundName= UILocalNotificationDefaultSoundName;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadData" object:self];
    }
}

//-(void)showNotification
//{
//    
//    NSDate *today = [NSDate date];
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"HH:mm"];
//    NSArray *currentTime = [[dateFormatter stringFromDate:today] componentsSeparatedByString:@" "];
//    NSArray *currentHourMinute = [[currentTime objectAtIndex:0] componentsSeparatedByString:@":"];
//    float hour = [[currentHourMinute objectAtIndex:0] floatValue]*60;
//    float minute = [[currentHourMinute objectAtIndex:1] floatValue];
//    if((1020 - (hour+minute))>0){        // 17 * 60 = 1020 i.e 5 pm
//        int time = (1020 - (hour+minute))*60;
//        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
//        localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:time];
//        localNotification.alertBody= @"Please Fill TimeSheet";
//        localNotification.repeatInterval = 0;
//        localNotification.soundName= UILocalNotificationDefaultSoundName;
//        
//        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadData" object:self];
//    }
//    
//}

@end

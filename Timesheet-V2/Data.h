//
//  Data.h
//  Timesheet-V2
//
//  Created by Arun Jangid on 13/10/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
@interface Data : NSObject
@property (nonatomic, strong) User * currentUser;
+(instancetype)sharedInstance;
-(void)save;
@end

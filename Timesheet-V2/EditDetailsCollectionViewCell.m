//
//  EditDetailsCollectionViewCell.m
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 11/18/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "EditDetailsCollectionViewCell.h"
@implementation EditDetailsCollectionViewCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)setDatasource:(NSDictionary *)datasource
{
    dataSource = [[NSMutableDictionary alloc]initWithDictionary:datasource];
    [nameTf setText:datasource[@"name"]];
    int budget = (int)[datasource[@"budget"]integerValue]/60;
    [budgetTf setText:[NSString stringWithFormat:@"%d",budget]];
    [startDateTf setDropDownMode:IQDropDownModeDatePicker];
    [endDateTf setDropDownMode:IQDropDownModeDatePicker];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    [startDateTf setDateFormatter:dateFormatter];
    [endDateTf setDateFormatter:dateFormatter];
    NSDateFormatter *firebaseDateFormatter=[[NSDateFormatter alloc] init];
    [firebaseDateFormatter setDateFormat:@"d MMMM YYYY, EEEE"];
    [startDateTf setSelectedItem:[dateFormatter stringFromDate:[firebaseDateFormatter dateFromString:datasource[@"startDate"]]]];
    startDate = [firebaseDateFormatter dateFromString:datasource[@"startDate"]];
    endDate =[firebaseDateFormatter dateFromString:datasource[@"endDate"]];
    [endDateTf setSelectedItem:[dateFormatter stringFromDate:[firebaseDateFormatter dateFromString:datasource[@"endDate"]]]];

}

-(void)textField:(IQDropDownTextField *)textField didSelectDate:(NSDate *)date
{
    if (textField == startDateTf) {
        startDate = date;
        [endDateTf setMinimumDate:date];
    }
    else
    {
        endDate = date;
      //  [self.contentView setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    }
}

- (IBAction)updateDetailsClicked:(id)sender
{
    NSDateFormatter *firebaseDateFormatter=[[NSDateFormatter alloc] init];
    [firebaseDateFormatter setDateFormat:@"d MMMM YYYY, EEEE"];
    NSString *startDateString =[firebaseDateFormatter stringFromDate:startDate];
    NSString *endDateString =[firebaseDateFormatter stringFromDate:endDate];
    [dataSource setValue:nameTf.text forKey:@"name"];
    [dataSource setValue:@([budgetTf.text intValue] * 60) forKey:@"budget"];
    [dataSource setValue:startDateString forKey:@"startDate"];
    [dataSource setValue:endDateString forKey:@"endDate"];
    _didUpdateDetails(dataSource);
    
}
@end

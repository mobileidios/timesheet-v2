//
//  ProjectBudgetCell.h
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 10/24/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface ProjectBudgetCell : BaseTableViewCell
@property (strong, nonatomic) IBOutlet UILabel *totalLbl;
@property (strong, nonatomic) IBOutlet UILabel *allocatedBudgetLbl;

@end

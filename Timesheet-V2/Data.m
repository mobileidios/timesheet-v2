//
//  Data.m
//  Timesheet-V2
//
//  Created by Arun Jangid on 13/10/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "Data.h"
#import <AutoCoding/AutoCoding.h>
#import "Constant.h"
@import FirebaseAuth;
@import FirebaseDatabase;
static Data * instance = nil;
@implementation Data

+(instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [Data objectWithContentsOfFile:[filePath stringByAppendingPathComponent:@"Data"]];
        if (instance == nil) {
            instance = [Data new];
            [instance save];
        }
    });
    return instance;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        
    }
    return self;
}

-(void)save
{
    [self writeToFile:[filePath stringByAppendingPathComponent:@"Data"] atomically:YES];
}

@end

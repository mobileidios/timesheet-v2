//
//  AddFunctionsCollectionViewCell.h
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 11/18/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "BaseCollectionViewCell.h"

@interface AddFunctionsCollectionViewCell : BaseCollectionViewCell<UITableViewDelegate,UITableViewDataSource>
{

    IBOutlet UITableView *functionsTable;
     NSMutableDictionary *dataSource;
    NSMutableDictionary *allFunctions;
}
@property (nonatomic,strong)void(^didUpdateFunctions)(NSDictionary *);
-(void)setDatasource:(NSDictionary *)datasource;
@end

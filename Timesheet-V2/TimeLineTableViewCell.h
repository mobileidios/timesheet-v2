//
//  TimeLineTableViewCell.h
//  Timesheet-V2
//
//  Created by Sonali on 10/14/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeLineTableViewCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UIView   *addButtonView;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
- (IBAction)addButton:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *selectProjectView;
@property (weak, nonatomic) IBOutlet UICollectionView *selectProjectCollectionView;

@property (weak, nonatomic) IBOutlet UIView *selectActivityView;
@property (weak, nonatomic) IBOutlet UICollectionView *selectActivityCollectionView;

@property (weak, nonatomic) IBOutlet UIView  *finalView;
@property (weak, nonatomic) IBOutlet UILabel *finalViewRoleLabel;
@property (weak, nonatomic) IBOutlet UILabel *finalViewActivityLabel;
@property (weak, nonatomic) IBOutlet UILabel *finalViewProjectLabel;
@property (weak, nonatomic) IBOutlet UILabel *finalViewTimeLabel;

@property (weak, nonatomic) IBOutlet UIView *selectTimeView;
@property (weak, nonatomic) IBOutlet UILabel *selectTimeLabel;
@property (weak, nonatomic) IBOutlet UISlider *selectTimeSlider;
@property (strong, nonatomic) id projectListArray;
@property (strong, nonatomic) id projectListKeyArray;
@property (strong, nonatomic) id activityListArray;

@property (strong, nonatomic) NSMutableDictionary *timeLine;
@property (nonatomic,strong)void(^addTimeLine)(NSMutableDictionary*);
@property (nonatomic,strong)void(^updateTimeLineDuration)(int);
@property (nonatomic,strong)void(^updateTimeLabel)(NSString*);
@property (nonatomic,strong)int (^check24HourHandler)();
@property (nonatomic,strong)void(^timeEqual24Hour)();
@property (nonatomic,strong)int(^addtionOfPreviousTime)();
@end

//
//  HistoryTableViewCell.h
//  Timesheet-V2
//
//  Created by Sonali on 10/20/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *historyFinalRoleLabel;
@property (weak, nonatomic) IBOutlet UIView *historyFinalView;
@property (weak, nonatomic) IBOutlet UILabel *historyFinalActivityLabel;
@property (weak, nonatomic) IBOutlet UILabel *historyFinalProjectLabel;
@property (weak, nonatomic) IBOutlet UILabel *historyFinalTimeLabel;




@end

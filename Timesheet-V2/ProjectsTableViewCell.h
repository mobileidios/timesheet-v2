//
//  ProjectsTableViewCell.h
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 10/14/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface ProjectsTableViewCell : BaseTableViewCell


 @property(nonatomic,strong)IBOutlet UISwitch *projectSwitch;
 @property(nonatomic,strong)IBOutlet UILabel *projectName;
    

@property (nonatomic,strong)void(^changeStatusHandler)(BOOL);
@end

//
//  AddFunctionsViewController.m
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 10/13/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "AddFunctionsViewController.h"

@interface AddFunctionsViewController ()<UITableViewDelegate,UITableViewDataSource>
{

    IBOutlet UITextField *functionNameTextField;
    IBOutlet UITableView *functionsTableView;
}
@end

@implementation AddFunctionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self fetchFunctionsFromFirebase];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)fetchFunctionsFromFirebase
{
    [FIRDatabaseReference fetchSingleObjectForFirstChild:FUNCTION forType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (![snapshot.value isKindOfClass:[NSNull class]]) {
            self.datasource = snapshot.value;
            [functionsTableView reloadData];
        }
    }];

}
- (IBAction)addBtnClicked:(id)sender
{
   
    [functionNameTextField resignFirstResponder];
    if ([functionNameTextField hasText])
    {
        
    [FIRDatabaseReference addObjectForFirstChild:FUNCTION forObject:functionNameTextField.text withCompletionHandler:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref, NSString *lastPath) {
        [self fetchFunctionsFromFirebase];
         [functionNameTextField setText:@""];
    }];
    }
    else
    {
        [[SessionManager sharedManager]showAlertWithMessage:@"Enter a function name first" forAction:^{}];
    }
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ((![textField.text length]) && [string isEqualToString:@" "])
    {
        return NO;
        
    }
    return YES;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.datasource allValues] count];
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
 return @"Functions";
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [functionsTableView dequeueReusableCellWithIdentifier:@"UITableViewCell"];
    cell.textLabel.text = [[self.datasource allValues] objectAtIndex:indexPath.row];
    return cell;
}
//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
//    // Return YES if you want the specified item to be editable.
//    return YES;
//}
//
//// Override to support editing the table view.
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        [FIRDatabaseReference removeObjectWithoutAutoIDForFirstChild:FUNCTION_PATH([[self.datasource allKeys] objectAtIndex:indexPath.row]) withCompletionHandler:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref, NSString *lastPath) {
//            [self fetchFunctionsFromFirebase];
//        }];
//    }
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

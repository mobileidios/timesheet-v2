//
//  ProjectNameCollectionViewCell.h
//  Timesheet-V2
//
//  Created by Sonali on 10/14/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectNameCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *projectBackgroundColor;
@property (weak, nonatomic) IBOutlet UILabel *projectNameLabel;

@end

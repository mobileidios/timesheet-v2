//
//  NavigationViewController.h
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 10/13/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationViewController : UINavigationController
- (void)panGestureRecognized:(UIPanGestureRecognizer *)sender;
@end

//
//  AddResourcesCollectionViewCell.m
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 11/21/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "AddResourcesCollectionViewCell.h"
#import "AddResourcesTableViewCell.h"

@implementation AddResourcesCollectionViewCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)setDatasource:(NSDictionary *)datasource
{
  dataSource = [[NSMutableDictionary alloc]initWithDictionary:datasource];
    resources = [[NSMutableArray alloc]init];
    [FIRDatabaseReference fetchSingleObjectForFirstChild:USERS forType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (![snapshot.value isKindOfClass:[NSNull class]]) {
            allResources = [[NSMutableDictionary alloc]initWithDictionary:snapshot.value];
            [[dataSource[@"Resources"] allKeys]enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
             {
                 if ([[allResources allKeys] containsObject:obj]) {
                     
                     [allResources removeObjectForKey:obj];
                     
                 }
                 
             
             }];
            [self filterData];
        }
    }];
     

}
-(void)filterData
{
    
    [[allResources allValues]enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSMutableDictionary *resource = [[NSMutableDictionary alloc]initWithDictionary:obj];
        [resource setObject:[[allResources allKeys] objectAtIndex:idx] forKey:@"resourceKey"];
        [resources addObject:resource];
    }];
    NSSortDescriptor * nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSArray * sortDescriptors = [NSArray arrayWithObject:nameDescriptor];
    resources = (NSMutableArray *)[resources sortedArrayUsingDescriptors:sortDescriptors];
    //    [[self.datasource allKeys]enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
    //
    //    }];
    [resourcesTable reloadData];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AddResourcesTableViewCell *resCell = [resourcesTable cellForRowAtIndexPath:indexPath];
    [resCell.resourceStatusTF becomeFirstResponder];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [resources count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AddResourcesTableViewCell *resCell = [resourcesTable dequeueReusableCellWithIdentifier:@"AddResourcesTableViewCell" forIndexPath:indexPath];
    resCell.resourceName.text = [[resources objectAtIndex:indexPath.row]valueForKey:@"name"];
    [resCell.resourceStatusTF setItemList:[[dataSource[@"functions"]allValues]valueForKey:@"name"]];
    NSDictionary * temp = [dataSource[@"Resources"] valueForKey:[[resources objectAtIndex:indexPath.row]valueForKey:@"resourceKey"]];
    if (temp) {
        [resCell.resourceStatus setText:temp[@"role"]];
        [resCell.resourceStatus setTextColor:[UIColor greenColor]];
    }

    resCell.didSelectResource = ^(NSString * role){
      __block  NSString *functKey = [[NSString alloc]init];
        [[dataSource[@"functions"]allValues]enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
        {
            if ([obj[@"name"] isEqualToString:role]) {
                functKey = [[dataSource[@"functions"]allKeys]objectAtIndex:idx];
            }
        }];
        NSMutableDictionary *projectResources = [[NSMutableDictionary alloc]init];
        [projectResources setValue:[[resources objectAtIndex:indexPath.row]valueForKey:@"name"] forKey:@"name"];
        [projectResources setValue:[[resources objectAtIndex:indexPath.row]valueForKey:@"email"] forKey:@"email"];
        [projectResources setValue:@0 forKey:@"consumed"];
        [projectResources setValue:role forKey:@"role"];
        [projectResources setValue:functKey forKey:@"roleKey"];
        [dataSource[@"Resources"] setObject:projectResources forKey:[[resources objectAtIndex:indexPath.row]valueForKey:@"resourceKey"]];
        
        
    };
    resCell.didDeselectResource = ^(){
        [dataSource[@"Resources"] removeObjectForKey:[[resources objectAtIndex:indexPath.row]valueForKey:@"resourceKey"]];
    };

    return resCell;
}
- (IBAction)addResourcesClicked:(id)sender
{
      _didUpdateResources(dataSource);
}

@end

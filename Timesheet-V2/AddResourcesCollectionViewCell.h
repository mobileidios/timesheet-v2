//
//  AddResourcesCollectionViewCell.h
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 11/21/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "BaseCollectionViewCell.h"

@interface AddResourcesCollectionViewCell : BaseCollectionViewCell<UITableViewDelegate,UITableViewDataSource>
{

    IBOutlet UITableView *resourcesTable;
     NSMutableDictionary *dataSource;
    NSMutableDictionary *allResources;
    NSMutableArray *resources;
}
@property (nonatomic,strong)void(^didUpdateResources)(NSDictionary *);
-(void)setDatasource:(NSDictionary *)datasource;
@end

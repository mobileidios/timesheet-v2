//
//  DashboardProjectViewController.m
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 10/18/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "DashboardProjectViewController.h"
#import "DashboardProjectCell.h"
#import "AllocateFunctionsViewController.h"

@interface DashboardProjectViewController ()<UITableViewDataSource,UITableViewDelegate>
{

    IBOutlet UITableView *resourceTable;
    NSMutableArray *functionsDict;
    NSMutableArray *resourceDict;
    BOOL resourceView;
    UIBarButtonItem * editBtn;
   
}
@end

@implementation DashboardProjectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    resourceView = YES;
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
//    editBtn = [[UIBarButtonItem alloc]initWithTitle:@"EDIT" style:UIBarButtonItemStylePlain target:self action:@selector(editClicked)];
       resourceDict = [[NSMutableArray alloc]initWithArray:[self.projDict[@"Resources"]allValues]];
    functionsDict = [[NSMutableArray alloc]initWithArray:[self.projDict[@"functions"]allValues]];
    self.title = self.projDict[@"name"];
   self.navigationController.navigationItem.backBarButtonItem.title = @"Back";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)changedParameter:(UISegmentedControl *)sender {
    if (resourceView) {
        resourceView = NO;
        [resourceTable reloadData];
    }
    else
    {
        resourceView = YES ;
        [resourceTable reloadData];
    }

}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (resourceView) {
        return [resourceDict count];
    }
    else
    {
        return [functionsDict count];
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    DashboardProjectCell *dasProjcell = [resourceTable dequeueReusableCellWithIdentifier:@"DashboardProjectCell" forIndexPath:indexPath];
    if (resourceView) {
        dasProjcell.resourceName.text = [[resourceDict objectAtIndex:indexPath.row]valueForKey:@"name"];
        float consumed = ([[[resourceDict objectAtIndex:indexPath.row]valueForKey:@"consumed"]floatValue]/60);
        float alloted  = 0.0 ;
        for (NSDictionary * dict in functionsDict) {
            if ([dict[@"name"] isEqualToString:[[resourceDict objectAtIndex:indexPath.row]valueForKey:@"role"]]) {
                alloted = ([dict[@"budget"] floatValue]/60);
                NSLog(@"%.2f",alloted);
            }
        }
        float consumedPer;
        if (alloted == 0) {
            consumedPer = 0;
        }
        else
        {
            consumedPer  = (consumed/alloted) * 100 ;
        }
        dasProjcell.consumedLbl.text = [NSString stringWithFormat:@"%d/%d",(int)consumed,(int)alloted];
        [dasProjcell layoutIfNeeded];
        NSLog(@"%f",(dasProjcell.allocatedView.frame.size.width));
        dasProjcell.consumedConstraint.constant = (consumedPer / 100) *(dasProjcell.allocatedView.frame.size.width);
        
    }
    else
    {
        dasProjcell.resourceName.text = [[functionsDict objectAtIndex:indexPath.row]valueForKey:@"name"];
        float consumed = ([[[functionsDict objectAtIndex:indexPath.row]valueForKey:@"consumed"]floatValue]/60);
        float alloted = ([[[functionsDict objectAtIndex:indexPath.row]valueForKey:@"budget"]floatValue]/60);
        float consumedPer;
        if (alloted == 0) {
            consumedPer = 0;
        }
        else
        {
       consumedPer  = (consumed/alloted) * 100 ;
        }
        NSLog(@"%f",(consumedPer / 100) *(dasProjcell.allocatedView.frame.size.width));
        [dasProjcell layoutIfNeeded];
        dasProjcell.consumedConstraint.constant = (consumedPer / 100) *(dasProjcell.allocatedView.frame.size.width);
        dasProjcell.consumedLbl.text = [NSString stringWithFormat:@"%d/%d",(int)consumed,(int)alloted];
    }
    return dasProjcell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  ProjectsViewController.m
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 10/13/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "ProjectsViewController.h"
#import "ProjectsTableViewCell.h"
#import "EditProjectViewController.h"

@interface ProjectsViewController ()<UITableViewDelegate,UITableViewDataSource>
{

    IBOutlet UITableView *projectsTableView;
}
@end

@implementation ProjectsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self fetchProjectsFromFirebase];
    
}
-(void)fetchProjectsFromFirebase
{
  [FIRDatabaseReference fetchSingleObjectForFirstChild:PROJECTS forType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
      if (![snapshot.value isKindOfClass:[NSNull class]]) {
          self.datasource = snapshot.value ;
          [projectsTableView reloadData];
      }

      
  }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.datasource allValues] count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProjectsTableViewCell *projCell = [projectsTableView dequeueReusableCellWithIdentifier:@"ProjectsTableViewCell" forIndexPath:indexPath];
    projCell.projectName.text = [[[self.datasource allValues] objectAtIndex:indexPath.row]valueForKey:@"name"];
    if ([[[[self.datasource allValues] objectAtIndex:indexPath.row]valueForKey:@"status"] isEqualToString:@"Live"]) {
         [projCell.projectSwitch setOn:YES];
    }
    else
    {
     [projCell.projectSwitch setOn:NO];
    }
   
    projCell.changeStatusHandler = ^(BOOL status){
        if (status) {
            [FIRDatabaseReference addObjectWithoutAutoIDForFirstChild:CHANGE_STATUS([[self.datasource allKeys] objectAtIndex:indexPath.row]) forObject:@"Live"];
        }
        else
        {
       [FIRDatabaseReference addObjectWithoutAutoIDForFirstChild:CHANGE_STATUS([[self.datasource allKeys] objectAtIndex:indexPath.row]) forObject:@"Closed"];
        }
    
    };
    
    return projCell;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [FIRDatabaseReference removeObjectWithoutAutoIDForFirstChild:PROJECT_PATH([[self.datasource allKeys] objectAtIndex:indexPath.row]) withCompletionHandler:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref, NSString *lastPath) {
             [self fetchProjectsFromFirebase];
        }];
    }
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"editProject"]) {
    EditProjectViewController *editCont = segue.destinationViewController;
    editCont.projectKey = [[self.datasource allKeys]objectAtIndex:[projectsTableView indexPathForSelectedRow].row];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

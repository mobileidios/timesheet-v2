//
//  User.h
//  Timesheet-V2
//
//  Created by Arun Jangid on 13/10/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject
@property (nonatomic, strong) NSString * userName;
@property (nonatomic, strong) NSString * userEmail;
@property (nonatomic, strong) NSString * userUid;
@property (nonatomic, strong) NSString * userType;

+(void)loadUser;
+(void)clearUser;
-(void)saveUserWithData:(id)response;

@end

//
//  AllocateResourcesTableViewCell.h
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 10/17/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface AllocateResourcesTableViewCell : BaseTableViewCell<IQDropDownTextFieldDataSource,IQDropDownTextFieldDelegate>
@property (strong, nonatomic) IBOutlet UILabel *resourceName;
@property (strong, nonatomic) IBOutlet UILabel *resourceStatus;
@property (strong, nonatomic) IBOutlet IQDropDownTextField *availableFunctionsField;
@property (nonatomic,strong)void(^didSelectResource)(NSString *);
@property (nonatomic,strong)void(^didDeselectResource)();
@end

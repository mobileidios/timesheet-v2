//
//  AddResourcesTableViewCell.h
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 11/21/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface AddResourcesTableViewCell : BaseTableViewCell<IQDropDownTextFieldDelegate,IQDropDownTextFieldDataSource>
@property (strong, nonatomic) IBOutlet UILabel *resourceName;
@property (strong, nonatomic) IBOutlet UILabel *resourceStatus;
@property (strong, nonatomic) IBOutlet IQDropDownTextField *resourceStatusTF;

@property (nonatomic,strong)void(^didSelectResource)(NSString *);
@property (nonatomic,strong)void(^didDeselectResource)();

@end

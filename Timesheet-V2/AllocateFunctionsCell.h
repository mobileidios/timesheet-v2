//
//  AllocateFunctionsCell.h
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 10/19/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface AllocateFunctionsCell : BaseTableViewCell<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UILabel *funcName;
@property (strong, nonatomic) IBOutlet UITextField *funcAllocation;
@property (nonatomic,strong)void(^didUpdateBudget)(int);
@property (nonatomic,strong)void(^hideDoneBtn)();
@end

//
//  AllocateFunctionsCell.m
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 10/19/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "AllocateFunctionsCell.h"

@implementation AllocateFunctionsCell


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    self.didUpdateBudget( [textField.text intValue]);
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{

    self.hideDoneBtn();
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

//
//  TimeLineViewController.m
//  Timesheet-V2
//
//  Created by Arun Jangid on 13/10/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "TimeLineViewController.h"
#import "DaysCollectionViewCell.h"
#import "TimeLineTableViewCell.h"
@interface TimeLineViewController ()
{
  NSArray *dayNames;// Su Mo Tu We Th Fr Sa
  NSArray * allDatesOfThisWeek; //contains whole week days and dates
  NSString *selectedDate;// stores selected date
  NSMutableArray *timelineDataValue;
  NSMutableArray *timelineDataKey;
    NSMutableArray *resourceArray;
    NSMutableArray *functionArray;
  __block int dayTimeTotal;
  __block int dayTimeTotalExcludingCurrent;
    IBOutlet UIView *progressBar;
    IBOutlet UIView *progressBarContainerView;
    Boolean timeEqual24;
}
@end

@implementation TimeLineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    timelineDataKey = [[NSMutableArray alloc]init];
    timelineDataValue = [[NSMutableArray alloc]init];
    resourceArray = [[NSMutableArray alloc]init];
    functionArray = [[NSMutableArray alloc]init];
    dayNames = [[NSArray alloc]initWithObjects:@"Su",@"Mo",@"Tu",@"We",@"Th",@"Fr",@"Sa",nil];
    allDatesOfThisWeek = [[SessionManager sharedManager] daysThisWeek];
    timeEqual24 = false; 
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [progressBar setFrame:CGRectMake(progressBar.frame.origin.x, progressBar.frame.origin.y,0, progressBar.frame.size.height)];
    [allDatesOfThisWeek enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"d MMMM YYYY, EEEE"];
        if([obj containsString:[dateFormatter stringFromDate:[NSDate date]]]){
            _dateLabel.text = obj;
            selectedDate = obj;
            [self fetchDataFromFireBaseForDate:obj];
            NSIndexPath *path = [NSIndexPath indexPathForItem:idx inSection:0];
            DaysCollectionViewCell *cell = (DaysCollectionViewCell *)[_daysCollectionView cellForItemAtIndexPath:path];
            [cell setBackgroundColor:[UIColor whiteColor]];
            [cell.dayLabel setTextColor:[HXColor hx_colorWithHexRGBAString:@"3f4055"]];
            [_daysCollectionView selectItemAtIndexPath:path animated:YES scrollPosition:UICollectionViewScrollPositionNone];
        }
    }];
    [self checkTimeIs24];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return dayNames.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    DaysCollectionViewCell *cell = [_daysCollectionView dequeueReusableCellWithReuseIdentifier:@"DaysCollectionViewCell" forIndexPath:indexPath];
    cell.dayLabel.text = [dayNames objectAtIndex:indexPath.row];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((CGRectGetWidth(self.view.frame)/7),_daysCollectionView.frame.size.height);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    DaysCollectionViewCell *cell = (DaysCollectionViewCell *)[_daysCollectionView cellForItemAtIndexPath:indexPath];
    [cell setBackgroundColor:[UIColor whiteColor]];
    [cell.dayLabel setTextColor:[HXColor hx_colorWithHexRGBAString:@"3f4055"]];
    _dateLabel.text = [allDatesOfThisWeek objectAtIndex:indexPath.row];
    selectedDate = [allDatesOfThisWeek objectAtIndex:indexPath.row];
    timelineDataValue = [[NSMutableArray alloc]init];
    timelineDataKey = [[NSMutableArray alloc]init];
    [_timeLineTableView reloadData];
    _timelabel.text = @"0:00";
    [progressBar setFrame:CGRectMake(progressBar.frame.origin.x, progressBar.frame.origin.y,0, progressBar.frame.size.height)];
    [self fetchDataFromFireBaseForDate:selectedDate];
}

-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    DaysCollectionViewCell *cell = (DaysCollectionViewCell *)[_daysCollectionView cellForItemAtIndexPath:indexPath];
    [cell setBackgroundColor:[HXColor hx_colorWithHexRGBAString:@"3f4055"]];
    [cell.dayLabel setTextColor:[UIColor whiteColor]];
}

-(void)checkTimeIs24
{
    if([self.timelabel.text isEqualToString:@"23:45"]){
        timeEqual24 = true;
        [_timeLineTableView reloadData];
    }
    else timeEqual24 = false;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(timeEqual24 == true)
        return timelineDataValue.count;
    else
        return timelineDataValue.count + 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    __weak TimeLineTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TimeLineTableViewCell" forIndexPath:indexPath];
    [cell setAddtionOfPreviousTime:^int{
        NSLog(@"indexPath.row %ld",(long)indexPath.row);
        if(indexPath.row == [timelineDataValue count]) // timeline array count != 0 and assaign total value to dayTimeTotalExcludingCurrent
        {
            if([timelineDataValue count] != 0)
            {
                dayTimeTotalExcludingCurrent = dayTimeTotal;
                return dayTimeTotal;
            }
        }
        return 0;
    }];
    [cell setCheck24HourHandler:^int{
        NSLog(@"timelineDataValue.count %lu",(unsigned long)[timelineDataValue count]);
        timeEqual24 = false;
        int hour2 = dayTimeTotalExcludingCurrent/60;
        int min2  = dayTimeTotalExcludingCurrent%60;
        int timeInMin2 = (hour2 * 60) + min2;
        return timeInMin2;
    }];
    [cell setTimeEqual24Hour:^{
        [self checkTimeIs24];
    }];
    [cell setUpdateTimeLabel:^(NSString *time) {
        cell.selectTimeLabel.text = time;
        int hour1 = [[[time componentsSeparatedByString:@":"] firstObject] intValue]*60;
        float min1  = [[[time componentsSeparatedByString:@":"] lastObject] intValue]/100.0;
        int timeInMin1 = hour1  + (min1 * 100);
        int hour2 = dayTimeTotalExcludingCurrent/60;
        int min2  = dayTimeTotalExcludingCurrent%60;
        int timeInMin2 = (hour2 * 60) + min2;
        int totalTimeInMin = timeInMin1 + timeInMin2;
        int hour = totalTimeInMin/60;
        int min  = totalTimeInMin%60;
        if(((hour * 60)+min) < 1440){
            if(min > 9){
                _timelabel.text = [NSString stringWithFormat:@"%d:%2d",hour,min];
            }else{
                _timelabel.text = [NSString stringWithFormat:@"%d:0%d",hour,min];
            }
            [self setProgressBarWith:hour And:min];
        }else{
            [cell.selectTimeSlider setValue:cell.selectTimeSlider.value];
        }
    }];
    
    if(indexPath.row < timelineDataValue.count){
        [cell.finalView setHidden:NO];
        [cell.addButtonView setHidden:YES];
        [cell.selectActivityView setHidden:YES];
        [cell.selectProjectView setHidden:YES];
        [cell.selectTimeView setHidden:YES];
        [cell setUpdateTimeLineDuration:^(int timeInMin) {
            if(timeInMin < 1440 && [timelineDataKey count]!=0){
                int tempTimeToSubtract = [[timelineDataValue objectAtIndex:indexPath.row][@"duration"] intValue];
                [FIRDatabaseReference addObjectWithoutAutoIDForFirstChild:[[[USERS_TIMELINE([Data sharedInstance].currentUser.userUid) stringByAppendingPathComponent:selectedDate] stringByAppendingPathComponent:(NSString *)[timelineDataKey objectAtIndex:indexPath.row]] stringByAppendingPathComponent:@"duration"] forObject:@(timeInMin) withCompletionHandler:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref, NSString *lastPath) {
                    [FIRDatabaseReference fetchSingleObjectForFirstChild:[[[PROJECT_PATH([timelineDataValue objectAtIndex:indexPath.row][@"projectId"]) stringByAppendingPathComponent:@"Resources"] stringByAppendingPathComponent:[Data sharedInstance].currentUser.userUid] stringByAppendingPathComponent:@"consumed"] forType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                        if([timelineDataKey count]!=0){
                        [FIRDatabaseReference addObjectWithoutAutoIDForFirstChild:[[[PROJECT_PATH([timelineDataValue objectAtIndex:indexPath.row][@"projectId"]) stringByAppendingPathComponent:@"Resources"] stringByAppendingPathComponent:[Data sharedInstance].currentUser.userUid] stringByAppendingPathComponent:@"consumed"] forObject:@(timeInMin + ([snapshot.value intValue] - tempTimeToSubtract)) withCompletionHandler:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref, NSString *lastPath) {
                            if([timelineDataKey count]!=0){
                            [FIRDatabaseReference fetchSingleObjectForFirstChild:[[[PROJECT_PATH([timelineDataValue objectAtIndex:indexPath.row][@"projectId"]) stringByAppendingPathComponent:@"functions"] stringByAppendingPathComponent:[timelineDataValue objectAtIndex:indexPath.row][@"projectRoleKey"]] stringByAppendingPathComponent:@"consumed"] forType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {       
                                [FIRDatabaseReference addObjectWithoutAutoIDForFirstChild:[[[PROJECT_PATH([timelineDataValue objectAtIndex:indexPath.row][@"projectId"]) stringByAppendingPathComponent:@"functions"] stringByAppendingPathComponent:[timelineDataValue objectAtIndex:indexPath.row][@"projectRoleKey"]] stringByAppendingPathComponent:@"consumed"] forObject:@(timeInMin + ([snapshot.value intValue] - tempTimeToSubtract)) withCompletionHandler:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref, NSString *lastPath) {
                                    [self fetchDataFromFireBaseForDate:selectedDate];
                                }];
                            }];
                            }
                        }];
                        }
                    }];
                }];
            }
        }];
        cell.finalViewRoleLabel.text = [[timelineDataValue objectAtIndex:indexPath.row] valueForKey:@"projectRole"];
        cell.finalViewActivityLabel.text = [[timelineDataValue objectAtIndex:indexPath.row] valueForKey:@"activityName"];
        int hour = [[[timelineDataValue objectAtIndex:indexPath.row] valueForKey:@"duration"] intValue]/60;
        int min  = [[[timelineDataValue objectAtIndex:indexPath.row] valueForKey:@"duration"] intValue]%60;
         if(((hour * 60)+min) < 1440){
             float total = [[NSString stringWithFormat:@"%d.%d",hour,min] floatValue];
             [cell.selectTimeSlider setValue:total];
             if(min > 9){
                 cell.finalViewTimeLabel.text = [NSString stringWithFormat:@"%d:%d",hour,min];
                 cell.selectTimeLabel.text = [NSString stringWithFormat:@"%d:%d",hour,min];
             }else{
                 cell.finalViewTimeLabel.text = [NSString stringWithFormat:@"%d:0%d",hour,min];
                 cell.selectTimeLabel.text = [NSString stringWithFormat:@"%d:0%d",hour,min];
             }
         }else{
             [cell.selectTimeSlider setValue:cell.selectTimeSlider.value];
         }
        cell.finalViewProjectLabel.text = [[timelineDataValue objectAtIndex:indexPath.row] valueForKey:@"projectName"];
        [cell layoutIfNeeded];
    }else{
        [cell.finalView setHidden:NO];
        [cell.addButtonView setHidden:NO];
        [cell.selectActivityView setHidden:NO];
        [cell.selectProjectView setHidden:NO];
        [cell.selectTimeView setHidden:NO];
        [cell layoutIfNeeded];
        [cell setAddTimeLine:^(NSMutableDictionary *data) {
            [FIRDatabaseReference addObjectForFirstChild:[USERS_TIMELINE([Data sharedInstance].currentUser.userUid) stringByAppendingPathComponent:selectedDate] forObject:data withCompletionHandler:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref, NSString *lastPath) {
                [FIRDatabaseReference fetchSingleObjectForFirstChild:[[[PROJECT_PATH(data[@"projectId"]) stringByAppendingPathComponent:@"Resources"] stringByAppendingPathComponent:[Data sharedInstance].currentUser.userUid] stringByAppendingPathComponent:@"consumed"] forType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                    [FIRDatabaseReference addObjectWithoutAutoIDForFirstChild:[[[PROJECT_PATH(data[@"projectId"]) stringByAppendingPathComponent:@"Resources"] stringByAppendingPathComponent:[Data sharedInstance].currentUser.userUid] stringByAppendingPathComponent:@"consumed"] forObject:@([data[@"duration"] intValue] + [snapshot.value intValue]) withCompletionHandler:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref, NSString *lastPath) {
                        [FIRDatabaseReference fetchSingleObjectForFirstChild:[[[PROJECT_PATH(data[@"projectId"]) stringByAppendingPathComponent:@"functions"]  stringByAppendingPathComponent:data[@"projectRoleKey"]] stringByAppendingPathComponent:@"consumed"] forType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                            [FIRDatabaseReference addObjectWithoutAutoIDForFirstChild:[[[PROJECT_PATH(data[@"projectId"]) stringByAppendingPathComponent:@"functions"] stringByAppendingPathComponent:data[@"projectRoleKey"]] stringByAppendingPathComponent:@"consumed"] forObject:@([data[@"duration"] intValue] + [snapshot.value intValue]) withCompletionHandler:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref, NSString *lastPath) {
                                [self fetchDataFromFireBaseForDate:selectedDate];
                            }];
                        }];
                    }];
                }];
            }];
        }];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TimeLineTableViewCell *cell = [_timeLineTableView cellForRowAtIndexPath:indexPath];
    if(![cell.finalView isHidden]){
        if(![cell.addButtonView isHidden]){
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            return;
        }
        else if(![cell.selectProjectView isHidden]){
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            return;
        }
        else if(![cell.selectTimeView isHidden]){
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            return;
        }
        if([timelineDataValue count]!=0){
        if(indexPath.row < [timelineDataValue count])
        dayTimeTotalExcludingCurrent = dayTimeTotal - [[timelineDataValue objectAtIndex:indexPath.row][@"duration"] intValue];
        }
        cell.selectActivityView.hidden = YES;
        cell.selectProjectView.hidden = YES;
        cell.selectTimeView.hidden = NO;
        cell.addButtonView.hidden = YES;
        cell.finalView.hidden = YES;
    }
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    TimeLineTableViewCell *cell = [_timeLineTableView cellForRowAtIndexPath:indexPath];
    cell.selectActivityView.hidden = YES;
    cell.selectProjectView.hidden = YES;
    cell.selectTimeView.hidden = YES;
    cell.addButtonView.hidden = YES;
    cell.finalView.hidden = NO;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
      TimeLineTableViewCell *cell = [_timeLineTableView cellForRowAtIndexPath:indexPath];
     if(![cell.finalView isHidden]){
         if(![cell.addButtonView isHidden]){
             return NO;
         }
         if(![cell.selectProjectView isHidden]){
             return NO;
         }
         if(![cell.selectTimeView isHidden]){
             return NO;
         }
         return YES;
     }else{
         return NO;
     }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    TimeLineTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if([timelineDataKey count]!=0){
            [FIRDatabaseReference fetchSingleObjectForFirstChild:[[[PROJECT_PATH([timelineDataValue objectAtIndex:indexPath.row][@"projectId"]) stringByAppendingPathComponent:@"Resources"] stringByAppendingPathComponent:[Data sharedInstance].currentUser.userUid] stringByAppendingPathComponent:@"consumed"] forType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                NSArray * array = [cell.selectTimeLabel.text componentsSeparatedByString:@":"];
                int min = [[array objectAtIndex:0] intValue] * 60 + [[array objectAtIndex:1]intValue];
                [FIRDatabaseReference addObjectWithoutAutoIDForFirstChild:[[[PROJECT_PATH([timelineDataValue objectAtIndex:indexPath.row][@"projectId"]) stringByAppendingPathComponent:@"Resources"] stringByAppendingPathComponent:[Data sharedInstance].currentUser.userUid] stringByAppendingPathComponent:@"consumed"] forObject:@([snapshot.value intValue] - min) withCompletionHandler:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref , NSString *lastPath) {
                    [FIRDatabaseReference fetchSingleObjectForFirstChild:[[[PROJECT_PATH([timelineDataValue objectAtIndex:indexPath.row][@"projectId"]) stringByAppendingPathComponent:@"functions"]  stringByAppendingPathComponent:[timelineDataValue objectAtIndex:indexPath.row][@"projectRoleKey"]] stringByAppendingPathComponent:@"consumed"] forType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshotFunction) {
                        [FIRDatabaseReference addObjectWithoutAutoIDForFirstChild:[[[PROJECT_PATH([timelineDataValue objectAtIndex:indexPath   .row][@"projectId"]) stringByAppendingPathComponent:@"functions"] stringByAppendingPathComponent:[timelineDataValue objectAtIndex:indexPath.row][@"projectRoleKey"]] stringByAppendingPathComponent:@"consumed"] forObject:@([snapshotFunction.value intValue] - min) withCompletionHandler:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref, NSString *lastPath) {
                            NSString *path = [[USERS_TIMELINE([Data sharedInstance].currentUser.userUid) stringByAppendingPathComponent:selectedDate] stringByAppendingPathComponent:[timelineDataKey objectAtIndex:indexPath.row]];
                            [FIRDatabaseReference removeObjectWithoutAutoIDForFirstChild:path withCompletionHandler:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref, NSString *lastPath) {
                                timeEqual24 = false;
                                [self fetchDataFromFireBaseForDate:selectedDate];
                            }];
                        }];
                    }];
                }];
            }];
        }
    }
}

-(void)fetchDataOfUserProject
{
    
    
   
}
-(void)fetchDataFromFireBaseForDate:(NSString *)date{
    [timelineDataValue removeAllObjects];
    [timelineDataKey removeAllObjects];
    [FIRDatabaseReference fetchSingleObjectForFirstChild:[USERS_TIMELINE([Data sharedInstance].currentUser.userUid) stringByAppendingPathComponent:selectedDate] forType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if(![snapshot exists])
        {
            self.timelabel.text = @"0:00";
            [progressBar setFrame:CGRectMake(progressBar.frame.origin.x, progressBar.frame.origin.y,0, progressBar.frame.size.height)];
        }
        else if([snapshot exists]){
            [timelineDataValue addObjectsFromArray:[snapshot.value allValues]];
            [timelineDataKey addObjectsFromArray:[snapshot.value allKeys]];
            dayTimeTotal = 0;
            [timelineDataValue enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                dayTimeTotal = dayTimeTotal + [obj[@"duration"] intValue];
            }];
            int hour = dayTimeTotal/60;
            int min  = dayTimeTotal%60;
            if(min > 9){
                _timelabel.text = [NSString stringWithFormat:@"%d:%d",hour,min];
                _timelabel.text = [NSString stringWithFormat:@"%d:%d",hour,min];
            }else{
                _timelabel.text = [NSString stringWithFormat:@"%d:0%d",hour,min];
                _timelabel.text = [NSString stringWithFormat:@"%d:0%d",hour,min];
            }
            [self setProgressBarWith:hour And:min];
        }
        [self checkTimeIs24];
        [self.timeLineTableView reloadData];
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"d MMMM YYYY, EEEE"];
        NSDate *collectionViewDate = [formatter dateFromString:selectedDate];
        NSDate *todaysDate = [formatter dateFromString:[formatter stringFromDate:[NSDate date]]];
        if ([collectionViewDate earlierDate:todaysDate] == collectionViewDate) {
            [_timeLineTableView setHidden:NO];            
        } else {
            //Date is EARLIER than today
            [_timeLineTableView setHidden:YES];
        }
    }];
}

-(void)setProgressBarWith:(int)hour And:(int)min{
    int timeInMinutes = (hour * 60) + min;
    int barWidth = (int)progressBarContainerView.frame.size.width;
    float percent = (timeInMinutes / 1440.0)*100.0;
    float setWidth = (percent/100)*barWidth;
    [progressBar setFrame:CGRectMake(progressBar.frame.origin.x, progressBar.frame.origin.y,setWidth, progressBar.frame.size.height)];
}
@end

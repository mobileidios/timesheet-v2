//
//  AllocateResourcesViewController.m
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 10/14/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "AllocateResourcesViewController.h"
#import "AllocateResourcesTableViewCell.h"
#import "ProjectsViewController.h"

@interface AllocateResourcesViewController ()<UITableViewDelegate,UITableViewDataSource>
{

    IBOutlet UITableView *allocateTable;
    NSMutableDictionary *selectedResources;
    NSMutableArray * functionsInTF;
    NSMutableArray *resources;
}
@end

@implementation AllocateResourcesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    resources = [[NSMutableArray alloc]init];
    functionsInTF = [[NSMutableArray alloc]initWithArray:[self.functDict allValues]];
    [functionsInTF addObject:@"Not Assigned"];
    // Do any additional setup after loading the view.
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    selectedResources = [[NSMutableDictionary alloc]init];
    [self fetchResourcesFromFirebase];
}
- (IBAction)addProjectClicked:(id)sender {
    if ([[selectedResources allKeys] count]) {
    [self.project setObject:selectedResources forKey:@"Resources"];
    [self.project setValue:@"NO" forKey:@"allocationDone"];
    NSLog(@"%@",self.project);
    [FIRDatabaseReference addObjectForFirstChild:PROJECTS forObject:self.project withCompletionHandler:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref, NSString *lastPath)
     {
         for (UIViewController * controller in self.navigationController.viewControllers) {
             if ([controller isKindOfClass:[ProjectsViewController class]]) {
        [self.navigationController popToViewController:controller animated:YES];
                 break;
             }
         }
     }];
    }
    else
    {
     [[SessionManager sharedManager]showAlertWithMessage:@"Please select resources for functions" forAction:^{}];
    
    }

}

-(void)fetchResourcesFromFirebase
{
    [FIRDatabaseReference fetchSingleObjectForFirstChild:USERS forType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (![snapshot.value isKindOfClass:[NSNull class]]) {
            self.datasource = snapshot.value ;
            [self filterData];
            
        }
    }];
    
}
-(void)filterData
{
    [[self.datasource allValues]enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSMutableDictionary *resource = [[NSMutableDictionary alloc]initWithDictionary:obj];
        [resource setObject:[[self.datasource allKeys] objectAtIndex:idx] forKey:@"resourceKey"];
        [resources addObject:resource];
    }];
  NSSortDescriptor * nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
  NSArray * sortDescriptors = [NSArray arrayWithObject:nameDescriptor];
   resources = (NSMutableArray *)[resources sortedArrayUsingDescriptors:sortDescriptors];
//    [[self.datasource allKeys]enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//       
//    }];
     [allocateTable reloadData];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AllocateResourcesTableViewCell *resourceCell = [allocateTable cellForRowAtIndexPath:indexPath];
    [resourceCell.availableFunctionsField becomeFirstResponder];

}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.datasource allValues] count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AllocateResourcesTableViewCell *resourceCell = [allocateTable dequeueReusableCellWithIdentifier:@"AllocateResourcesTableViewCell" forIndexPath:indexPath];
    resourceCell.resourceName.text = [[resources objectAtIndex:indexPath.row]valueForKey:@"name"];
    [resourceCell.availableFunctionsField setItemList:functionsInTF];
    NSDictionary * temp = [selectedResources valueForKey:[[resources objectAtIndex:indexPath.row]valueForKey:@"resourceKey"]];
    if (temp) {
        [resourceCell.resourceStatus setText:temp[@"role"]];
        [resourceCell.resourceStatus setTextColor:[UIColor greenColor]];
    }
    
    resourceCell.didSelectResource = ^(NSString * role){
        NSArray *functKey = [self.functDict allKeysForObject:role];
        NSMutableDictionary *projectResources = [[NSMutableDictionary alloc]init];
        [projectResources setValue:[[resources objectAtIndex:indexPath.row]valueForKey:@"name"] forKey:@"name"];
        [projectResources setValue:[[resources objectAtIndex:indexPath.row]valueForKey:@"email"] forKey:@"email"];
        [projectResources setValue:@0 forKey:@"consumed"];
        [projectResources setValue:role forKey:@"role"];
        [projectResources setValue:[functKey firstObject] forKey:@"roleKey"];
        [selectedResources setObject:projectResources forKey:[[resources objectAtIndex:indexPath.row]valueForKey:@"resourceKey"]];
        [allocateTable reloadData];

    };
    resourceCell.didDeselectResource = ^(){
        [selectedResources removeObjectForKey:[[resources objectAtIndex:indexPath.row]valueForKey:@"resourceKey"]];
    };
    return resourceCell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

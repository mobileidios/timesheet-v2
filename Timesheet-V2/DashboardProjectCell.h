//
//  DashboardProjectCell.h
//  Timesheet-V2
//
//  Created by Mayur Sojrani on 10/18/16.
//  Copyright © 2016 Arun Jangid. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface DashboardProjectCell : BaseTableViewCell
@property (strong, nonatomic) IBOutlet UILabel *resourceName;
@property (strong, nonatomic) IBOutlet UIView *allocatedView;
@property (strong, nonatomic) IBOutlet UILabel *consumedLbl;
@property (strong, nonatomic) IBOutlet UIView *consumedView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *consumedConstraint;

@end
